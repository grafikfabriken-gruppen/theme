��    m      �  �   �      @	  8   A	     z	  )   ~	  ;   �	     �	     �	     �	  "   	
     ,
     <
  "   K
     n
     ~
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                     (     6     F     X  	   ]  
   g     r     �  =   �  	   �     �     �     �               $     4     <     O     V     d     �     �     �  �   �     2     E     _     h     q  	        �     �     �     �     �  �   �     �     �     �  
   �  
   �     �  	   �  !   �          6  #   R     v     �     �     �     �     �     �  	   �  	   �     �     �     �  &        6  n   D  i   �          #     =     O     W     i     ~     �     �     �  '   �     �               :     R     ^  �  j  >   �     ;  '   ?  8   g     �     �     �      �     �     �      
     +     7  	   D  
   N     Y     ^     c     x          �     �     �  	   �  	   �  "   �     �     �          "     1     7     G     W     f  2   m     �     �     �     �     �     �     �                    $     3     N     S     c  u   j     �     �               #  	   2  	   <  
   F     Q     V     ]  �   s     =     T     j     r     x     �     �  "   �      �     �  '   �  !   #  !   E     g     �     �     �     �     �     �     �     �     �  &   �       u     d   �     �     �          '     5  %   O  *   u  $   �     �     �  .   �  "        2     C     ]     y     �     V   )             2          ;       X           7              /           (   J   0                 l   B              H   #       !      >   e   D   m       I           Z   j          	      L         *   `       +      -   W   4   A   T   g             &       N   R   ]   .       a           ,   \   F   3       "      [   E   K       :   
       '   =   P   _          5              G   M      %   ^   O   b   f   k               6   <         h          9       U                   S   8       c       C   i       d   Q   ?   1              Y      $   @    %sHere%s is onyl a few modern browsers that works great! 404 <strong>%s</strong> left to free shipping Add the klarna placeholder visual code from merchant enter. Background image Block Blogg archive Button 1 - Open link in new window Button 1 - text Button 1 - url Button 2 - Open link in new window Button 2 - text Button 2 - url Button text Button url Card Cards Choose an option Close Close (Esc) Contact Contact content Contact form Contacts Content Content after form Content left Content right Content two col Copyright content Date Direction Directions Displays on archive E-mail Enter a logo version that would go well on your primary color Error 404 Export contacts Fifty fifty image text Footer Fullwidth image Gallery carousel Gallery masonry General Go to the checkout Header Hero carousel I'd like to continue anyway Image Image description Images It looks like something went wrong here. The page you are looking for could not be found. Try reloading the page and make sure the URL is correct. Klarna placeholder Klarna placeholder script Location Logotype Logotype dark Menu menu Menu mobile Message Name Next (arrow right) No posts found Old browsers only have partial support for new technology and doesn't give the website an honset chance to live up to its fully potential. You can easily download an other web browser that works better. Open link in new window Outdated browser Phone Photo date Photo text Previous (arrow left) Read more Recommended imagesize 2560x1440px Recommended imagesize 700x700px Recommended imagesize 700xx Recommended minimum size: 500x500px Recommended size: 1920x500px Reverse section? Select block Select value Send message Slide Slides Sub Title Sub title Subject Subtitle Thank you content Thank you message after submitted form Thankyou page This content will be shown after the contact form. Perferably this will include the link to the privacy policy This message will be shown on the thank you-page. Use <code>{order_id}</code> to show the ID of the order Title Title inside contact form Toggle fullscreen Two col Update my browser Use dark menu color? Use dark text color? We could not find any posts.. Web production WooCommerce You need to run composer install first! You're using an old browser You've added You've reached free shipping Your input is not valid Zoom in/out in the cart Project-Id-Version: 
PO-Revision-Date: 2020-09-08 09:53+0200
Last-Translator: 
Language-Team: 
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_x:1,2c
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: vendor
X-Poedit-SearchPathExcluded-1: node_modules
X-Poedit-SearchPathExcluded-2: app/assets
X-Poedit-SearchPathExcluded-3: app/dist
X-Poedit-SearchPathExcluded-4: app/Components/Sections/Map/v1/node_modules
X-Poedit-SearchPathExcluded-5: app/Components/Sections/Map/v1/Dist
 %sHär%s är ett axplock moderna webbläsare som fungerar bra! 404 <strong>%s</strong> kvar till fri frakt Lägg till platshållarkod från klarna merchant center. Bakgrundsbild Återkommande sektion Nyhetsarkiv Knapp 1 - Öppna i nytt fönster Knapptext 1 Knapplänk 1 Knapp 2 - Öppna i nytt fönster Knapptext 2 Knapplänk 2 Knapptext Knapplänk Kort Kort Välj ett alternativ Stäng Stäng Kontakt Kontaktinnehåll Kontaktformulär Kontakter Innehåll Innehåll efter kontaktformuläret Innehåll vänster Innehåll höger Två kolumner med text Copyright text Datum Vägbeskrivning Vägbeskrivning Visa på arkiv E-post Ange en logotyp som fungerar väl på vit bakgrund Fel 404 Exportera kontakter 50/50 - Bild och text  Sidfot Fullbred bild Bildspel Galleri, masonry Generell Gå till kassan Sidhuvud Hero, Bildspel Jag vill fortsätta ändå Bild Bildbeskrivning Bilder Nu blev det inte riktigt rätt. Sidan du söker kunde inte hittas. Prova att ladda om sidan eller kontrollera URL:en. Klarna platshållare Klarna platshållare javascript Plats Logotyp Logotyp, mörk Huvudmeny Mobilmeny Meddelande Namn Nästa Inga inlägg hittades Gamla webbläsare har bara delvis stöd för ny teknik och ger inte webbplatsen en ärlig chans att leva upp till sin fulla potential. Du kan enkelt ladda ner en annan webbläsare som fungerar bättre. Öppna i nytt fönster Utdaterad webbläsare Telefon Datum Beskrivning Föregående Läs mer Rekommenderad storlek: 2560x1440px Rekommenderad storlek: 700x700px Rekommenderad storlek: 700x? Rekommenderad minsta storlek: 500x500px Rekommenderad storlek: 1920x500px Byt plats på bild och innehåll? Välj återkommande sektion Välj värde Skicka meddelande Bildspel Slides Underrubrik Underrubrik Rubrik Underrubrik Tackmeddelande Tackmeddelande efter skickat formulär Tacksida Det här innehållet visas efter kontaktformuläret. Med fördel bör detta inkludera en länk till integritetspolicy Det här meddelandet visas på tacksidan. Använd  <code>{order_id}</code> för att visa ordernumret Titel Rubrik i kontaktformulär Felskärmsläge Två kolumner Uppdatera min webbläsare Vill du använda mörk text på meny? Vill du använda mörk text på innehåll? Vi kunde inte hitta några nyheter.. Webbproduktion WooCommerce Du måste köra ‘composer install’ först! Du använder en gammal webbläsare Du har lagt till Du har uppnått fri frakt Ditt fält är inte giltigt Zooma in/ut i varukorgen 