<?php

if (!defined('ABSPATH')) {
	die(); // Silience is golden
}

/**
 * Autgenerated fields for Post Model
 * 
 * @return array
 */ 
return array(
	array(
		"key" => "post_field_excerpt",
		"name" => "excerpt",
		"type" => "wysiwyg",
		"label" => __("Displays on archive", "grafikfabriken")
	)
);