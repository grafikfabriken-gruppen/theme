<?php
namespace App\Models;

if (!defined('ABSPATH')) {
	die(); // Silience is golden
}

/**
 * Class Post
 * 
 * A model for the post
 * 
 * @class Post
 * @namespace \App\Models
 * @extends \App\Models\Base\Post_Base
 * @implements \JsonSerializable
 */ 
class Post extends \App\Models\Base\Post_Base implements \JsonSerializable
{


	/**
	 * Constructing Post
	 * 
	 * @param mixed $custom_cpt_class
	 */ 
	public function __construct($custom_cpt_class = false)
	{
		parent::__construct($custom_cpt_class);
	}

	/**
	 * Serialize
	 * 
	 * @return array
	 */ 
	public function jsonSerialize()
	{
		$parent_data = parent::jsonSerialize();
		return $parent_data;
	}


	public function get_component_data()
	{
		$data = parent::get_component_data();
		$data["content"] = $this->get_excerpt();
		$data["sub_title"] = $data["date"];
		$data["link_url"] = $this->get_permalink();
		$data["link_text"] = __('Read more', 'grafikfabriken');
		$data["image_id"] = get_post_thumbnail_id($this->id);
		$data["sub_title_tag"] = 'h4';
		$data["title_tag"] = 'h2';
		return $data;
	}

}