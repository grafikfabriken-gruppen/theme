<?php
namespace App\Models\Base;

if (!defined('ABSPATH')) {
	die(); // Silience is golden
}

/**
 * Class Post
 * 
 * A model for the post
 * 
 * @class Post_Base
 * @namespace \App\Models\Base
 * @extends \Custom_CPT_Model
 */ 
class Post_Base extends \Custom_CPT_Model
{


	/**
	 * connected post type
	 * 
	 * @var string
	 */ 
	public $post_type = "post";

	/**
	 * excerpt
	 * 
	 * @var string
	 */ 
	protected $excerpt;

	/**
	 * Datatypes for deserialtion
	 * 
	 * @var array
	 */ 
	public $dataTypes = array(
		"excerpt" => "string"
	);

	/**
	 * Available getters
	 * 
	 * @var array
	 */ 
	public $getters = array(
		"excerpt" => "get_excerpt"
	);

	/**
	 * Available setters for deserialtion
	 * 
	 * @var array
	 */ 
	public $setters = array(
		"excerpt" => "set_excerpt"
	);

	/**
	 * Constructing Post
	 * 
	 * @param mixed $custom_cpt_class
	 */ 
	public function __construct($custom_cpt_class = false)
	{
		parent::__construct($custom_cpt_class);
	}

	/**
	 * Get excerpt
	 * 
	 * @return string
	 */ 
	public function get_excerpt()
	{
		return $this->__get('excerpt');
	}

	/**
	 * Set excerpt
	 * 
	 * @param string $excerpt
	 * @return string
	 */ 
	public function set_excerpt($excerpt)
	{
		$this->excerpt = $excerpt;
		return $this->__set('excerpt', $excerpt);
	}

}