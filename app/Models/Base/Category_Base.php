<?php
namespace App\Models\Base;

if (!defined('ABSPATH')) {
	die(); // Silience is golden
}

/**
 * Class Category
 * 
 * A model for category
 * 
 * @class Category_Base
 * @namespace \App\Models\Base
 * @extends \Custom_Tax_Model
 */ 
class Category_Base extends \Custom_Tax_Model
{


	/**
	 * connected post type
	 * 
	 * @var string
	 */ 
	public $post_type = "post";

	/**
	 * Constructing Category
	 * 
	 * @param mixed $custom_cpt_class
	 */ 
	public function __construct($custom_cpt_class = false)
	{
		parent::__construct($custom_cpt_class);
	}

}