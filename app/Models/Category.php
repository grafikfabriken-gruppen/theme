<?php
namespace App\Models;

if (!defined('ABSPATH')) {
	die(); // Silience is golden
}

/**
 * Class Category
 * 
 * A model for category
 * 
 * @class Category
 * @namespace \App\Models
 * @extends \App\Models\Base\Category_Base
 * @implements \JsonSerializable
 */ 
class Category extends \App\Models\Base\Category_Base implements \JsonSerializable
{


	/**
	 * Constructing Category
	 * 
	 * @param mixed $custom_cpt_class
	 */ 
	public function __construct($custom_cpt_class = false)
	{
		parent::__construct($custom_cpt_class);
	}

	/**
	 * Serialize
	 * 
	 * @return array
	 */ 
	public function jsonSerialize()
	{
		$parent_data = parent::jsonSerialize();
		return $parent_data;
	}


}