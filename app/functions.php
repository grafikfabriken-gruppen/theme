<?php

namespace App;

use App\Classes\Disable_WP_Posts;

use function GF\Utils\bootstrap;


include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if( !\is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
    \switch_theme('twentytwenty');
    wp_die(__('You need ACF activated, switching to twentytwenty', 'grafikfabriken'));
} elseif( !\is_plugin_active( 'gf-companion/gf-companion.php' ) ) {
    \switch_theme('twentytwenty');
    wp_die(__('You need Companion, switching to twentytwenty', 'grafikfabriken'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('GF\Controllers\Bootstrap_Controller')) {
    if (!file_exists($composer = __DIR__ . '/../vendor/autoload.php')) {
        echo __('You need to run composer install first!');
        die();
    }

    require_once $composer;
}

load_theme_textdomain("grafikfabriken", get_stylesheet_directory() . "/../language");

//Booting theme
bootstrap();

/**
 * Disable WP Posts
 * 
 * @var \App\Classes\Disable_WP_Posts $disable_wp_post
 */
// $disable_wp_post = Disable_WP_Posts::getInstance();
// $disable_wp_post->init(
//     [
//         'posts' => 'disable', //write disable for disable
//         'comments' => 'disable', //write disable for disable
//         'author_page' => 'disable', //write disable for disable
//         'feeds' => 'disable' //write disable for disable
//     ]
// );

if (class_exists('\\App\\Controllers\\Post_Controller')) {

    /**
     * Controllers for the store locations
     *
     * @return \App\Controllers\Post_Controller
     */
    function postController()
    {
        return \App\Controllers\Post_Controller::getInstance();
    }
}


if (class_exists('\\App\\Controllers\\Category_Controller')) {

    /**
     * Controllers for the products
     *
     * @return \App\Controllers\Category_Controller
     */
    function categoryController()
    {
        return \App\Controllers\Category_Controller::getInstance();
    }
}

// Run the constructs
postController();
categoryController();

if (defined('THEME_TAG_MANAGER')) {

    function add_tag_manager()
    {
        if (WP_ENV != 'production') return;
?>
<!-- Google Tag Manager -->
<script>
(function(w, d, s, l, i) {
	w[l] = w[l] || [];
	w[l].push({
		'gtm.start': new Date().getTime(),
		event: 'gtm.js'
	});
	var f = d.getElementsByTagName(s)[0],
		j = d.createElement(s),
		dl = l != 'dataLayer' ? '&l=' + l : '';
	j.async = true;
	j.src =
		'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
	f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', '<?= THEME_TAG_MANAGER; ?>');
</script>
<!-- End Google Tag Manager -->

<?php
    }
    add_action('wp_head', __NAMESPACE__ . '\\add_tag_manager');


    function add_tag_manager_noscript()
    {
        if (WP_ENV != 'production') return;
    ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= THEME_TAG_MANAGER; ?>" height="0" width="0"
		style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php
    }
    \add_action('wp_body_open', __NAMESPACE__ . '\\add_tag_manager_noscript');
}

\add_shortcode('year', function ($args) {
    return date_i18n('Y');
});


/**
 * Only search for posts..
 */
if (!is_admin()) {
    function gf_search_filter($query)
    {
        if ($query->is_search) {
            $query->set('post_type', 'post');
        }
        return $query;
    }
    add_filter('pre_get_posts', __NAMESPACE__ . '\\gf_search_filter');
}

/**
 * Add abilty for editors and
 * administrators to edit the privace policy 
 * page. 
 */
add_action('map_meta_cap', __NAMESPACE__ . '\\gf_manage_privacy_options', 1, 4);
function gf_manage_privacy_options($caps, $cap, $user_id, $args)

{

    if (!is_user_logged_in()) return $caps;

    $user_meta = get_userdata($user_id);
    if (array_intersect(['editor', 'administrator'], $user_meta->roles)) {
        if ('manage_privacy_options' === $cap) {
            $manage_name = is_multisite() ? 'manage_network' : 'manage_options';
            $caps = array_diff($caps, [$manage_name]);
        }
    }
    return $caps;
}