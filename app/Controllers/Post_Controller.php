<?php

namespace App\Controllers;

class Post_Controller extends Abstract_Post_Type_Controller
{

    /**
     * Post type
     *
     * @var string
     */
    protected $post_type = 'post';

    /**
     * Acf controller
     *
     * @var ACF_Controller
     */
    public $acf_controller;


    /**
     * Get instance
     *
     * @return void
     */
    public function _construct()
    {


        /**
         * Hook Controller
         * 
         */
        $this->acf_controller = ACF_Controller::getInstance();

        add_filter('active_post_types', array($this, 'setup_post_type'), 10, 1);

        /**
         * Load fields
         * 
         */
        add_action('acf/init', array($this, 'load_fields'));
    }

    /**
     * Change model fot the WP Post objet (used by get_custom_model)
     *
     * @param array $post_types
     * @return array
     */
    public function setup_post_type($post_types)
    {
        $post_types['post'] = '\\App\\Models\\Post';
        return $post_types;
    }

    /**
     * Load acf fields
     *
     * @return void
     */
    public function load_fields(): void
    {
        $acf_key = "general_post_data";
        $fields = $this->acf_controller->get_acf_tab($acf_key, __('General', "grafikfabriken"), $this->get_post_type());
        $fields = array_merge($fields, require dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'Fields/Post_Fields.php');
        $this->acf_controller->add_local_field_group($fields, $acf_key, __('Post', 'grafikfabriken'), $this->get_post_type());
    }

}
