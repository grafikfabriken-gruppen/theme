<?php

namespace App\Controllers;

use \GF\Utils\Singleton;

class Category_Controller extends Singleton
{
    /**
     * Taxonomy name
     *
     * @var string
     */
    public $tax_name = "category";

    /**
     * ACF Controller
     *
     * @var \App\Controllers\ACF_Controller
     */
    public $acf_controller;

    /**
     * Constructor
     *
     * @author Jonathan Persson <jonathan@grafikfabriken.nu>
     * @version 1.0
     */
    public function _construct()
    {

        /**
         * Set acf controller
         * 
         */
        $this->acf_controller = ACF_Controller::getInstance();

        /**
         * Add custom taxonomy
         */
        add_filter('active_post_taxonomies', function($taxes){
            $taxes["category"] = "\\App\\Models\\Category";
            return $taxes;
        },10,1);
    }

    /**
     * Get the parent brands
     *
     * @return array
     */
    public function get_categories()
    {
        $args = array(
            'taxonomy' => $this->tax_name,
            'parent' => 0,
            'hide_empty' => 1
        );

        return get_custom_taxonomies($args);
    }

}
