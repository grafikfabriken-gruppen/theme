<?php 
namespace App\Controllers;

use Custom_Cpt_Model;

abstract class Abstract_Post_Type_Controller extends \GF\Utils\Singleton{

    /**
     * Get post type
     *
     * @var string
     */
    protected $post_type = "";

    /**
     * Singular post type name
     *
     * @var string
     */
    protected $singular_name = "";

    /**
     * Plural post type name
     *
     * @var string
     */
    protected $plural_name = "";

    /**
     * Post type model
     *
     * @var string
     */
    protected $model_name = "";

    /**
     * Post type arguments
     *
     * @var array
     */
    protected $args = [];


    /**
     * Get singular name
     *
     * @return string
     */
    public function get_singular_name():string{
        return $this->singular_name;
    }

    /**
     * Get plural name
     *
     * @return string
     */
    public function get_plural_name():string{
        return $this->plural_name;
    }

    /**
     * Get model name
     *
     * @return string
     */
    public function get_model_name():string{
        return $this->model_name;
    }

    /**
     * Get CPT Args
     *
     * @return array
     */
    public function get_args():array{
        return $this->args;
    }

    /**
     * Set singular name
     *
     * @param string $name
     * @return void
     */
    public function set_singular_name(string $name):void{
        $this->singular_name = $name;
    }

    /**
     * Set plural name
     *
     * @param string $name
     * @return void
     */
    public function set_plural_name(string $name):void{
        $this->plural_name = $name;
    }

    /**
     * Set model name
     *
     * @param string $name
     * @return void
     */
    public function set_model_name(string $name):void{
        $this->model_name = $name;
    }

    /**
     * Set CPT Args
     *
     * @param array $args
     * @return void
     */
    public function set_args(array $args):void{
        $this->args = $args;
    }

    /**
     * Set post type
     *
     * @param string $_post_type
     * @return void
     * 
     */
    public function set_post_type(string $_post_type){
        $this->post_type = $_post_type;
    }

    /**
     * Get post type
     *
     * @return string
     * 
     */
    public function get_post_type(){
        return $this->post_type;
    }

    /**
     * Get post
     *
     * @return \Custom_Cpt_Model
     */
    public function get_post(){

        if(is_singular($this->get_post_type())){
            return \get_custom_model();
        }

        return false;

    }

    /**
     * Get posts
     *
     * @param array $_args
     * 
     * @return \Custom_Cpt_Model
     * 
     */
    public function get_posts(array $_args = []){

        $args = \wp_parse_args($_args, array(
            "post_type" => $this->get_post_type(),
            "posts_per_page" => -1
        ));

        $models = \get_custom_models($args);

        return $models;

    }

    /**
     * Create post
     *
     * @param array $_args
     * @return \Custom_Cpt_Model
     */
    public function create_post(array $_args):Custom_Cpt_Model
    {

        $args = \wp_parse_args(
            $_args,
            array(
                "post_status" => 'publish',
                "post_type" => $this->get_post_type(),
                "meta_input" => array()
            )
        );

        
        $model = new \Custom_Cpt_Model($args);

        return $model;

    }

    /**
     * Gets posts from the main loop
     *
     * @return \Custom_Cpt_Model[]
     */
    public function get_posts_from_the_loop():array{
        
        $new_posts = [];

        if(have_posts()){
            global $posts;
            foreach($posts as $post){
                the_post();
                $new_posts[] = get_custom_model();
            }
        }

        wp_reset_postdata();

        return $new_posts;

    }

    /**
     * Delete post
     *
     * @param int $id
     * @param boolean $force
     * @return boolean
     */
    public function delete_post(int $id, $force = true){
        $result = wp_delete_post($id, true);
        return is_a($result, '\WP_Post') ? true : false;
    }

    /**
     * Register the post type
     *
     * @return void
     */
    public function register_post_type():void{
        \add_cpt(
            $this->get_model_name(),
            array(
                'name' => $this->get_post_type(),
                'singular' => $this->get_singular_name(),
                'plural' => $this->get_plural_name(),
                'args' => $this->get_args()
            )
        );

    }

}