<?php
namespace App\Controllers;

class ACF_Controller extends \GF\Utils\Singleton{


    /**
     * Get choices from dropdown
     *
     * @param string $field_key
     * @return array
     */
    public function get_choices_from_dropdown(string $field_key):array{

        $result = array();

        if($field = \get_field_object($field_key)){

            $types = isset($field["choices"]) ? $field["choices"] : [];
            if(array_has_items($types)){
                return $types;
            }

        }

        return $result;

    }


    /**
     * Get value from dropdown
     *
     * @param string $v
     * @param string $field_key
     * @return string
     */
    public function acf_get_value_from_dropdown(string $v, string $field_key):string
    {

        $choices = $this->get_choices_from_dropdown($field_key); 

        // dpr($choices,2);

        if (array_has_items($choices)) {
            return isset($choices[$v]) ? $choices[$v] : '';
        }

        return '';

    }

    /**
     * Get ACF Tab
     *
     * @param string $key
     * @param string $name
     * @param string $type
     * @return array
     */
    public function get_acf_tab(string $key, string $name, string $type):array
    {
        return array(
            array(
                'key' => sprintf("acf-generic-tab-%s-%s", $type, $key),
                'label' => $name,
                'name' => 'tab-' . $key,
                'type' => 'tab'
            )
        );
    }


    /**
     * Add local field group
     *
     * @param array $fields
     * @param string $key
     * @param string $title
     * @param string $param_value
     * @param string $param_type
     * @return void
     */
    public function add_local_field_group(array $fields, string $key, string $title, string $param_value, string $param_type='post_type'):void{

        $args = array(
            'key' => sprintf("acf-field-group-%s-%s", $param_value, $key),
            'title' => $title,
            'fields' => $fields,
            'location' => array(
                array(
                    array(
                        'param' => $param_type,
                        'operator' => '==',
                        'value' => $param_value,
                    ),
                ),
            ),
        );

        if ($param_value == 'nav_menu_item') {
            // dpr($args,2);
        }
        \acf_add_local_field_group($args);
        
    }

}