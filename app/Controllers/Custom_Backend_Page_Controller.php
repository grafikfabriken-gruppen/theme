<?php
namespace App\Controllers;

class Custom_Backend_Page_Controller extends \Singleton
{


    /**
     * Page title
     *
     * @var string
     */
    public $page_title;

    /**
     * Slug
     *
     * @var string
     */
    public $slug;

    /**
     * Allowed caps
     *
     * @var string
     */
    public $allowed_caps = null;

    /**
     * Icon
     *
     * @var string
     */
    public $icon = null;

    /**
     * Position
     *
     * @var string
     */
    public $position = null;

    /**
     * WP Page
     *
     * @var string
     */
    public $wp_page;

    /**
     * Columns
     *
     * @var integer
     */
    public $columns = 2;

    /**
     * Register menu and actions
     *
     * @return void
     */
    public function _construct()
    {
        add_action('admin_menu', array($this, 'add_to_admin_menu'));
        add_action('add_meta_boxes', array($this, 'add_meta_boxes'));
    }

    /**
     * Add to admin
     *
     * @return void
     */
    public function add_to_admin_menu():void
    {

        $this->wp_page = add_menu_page(
            $this->page_title, 
            $this->page_title, 
            $this->allowed_caps, 
            $this->slug, 
            array($this, 'render_page'), 
            $this->icon, 
            $this->position
        );

        //Adding load & admin footer actions for page
        add_action('load-' . $this->wp_page, array($this, 'before_render'), 9);
        add_action('admin_footer-' . $this->wp_page, array($this, 'footer_scripts'));
        // add_action('admin_print_scripts-' . $this->wp_page, array($this, 'add_custom_scripts'));
        add_action('admin_print_scripts', array($this, 'add_custom_scripts'));

    }

    /**
     * Hook in here to add custom scripts
     *
     * @return void
     */
    public function add_custom_scripts():void
    {}

    /**
     * Footer scripts
     *
     * @return void
     */
    public function footer_scripts():void
    {
        ?>
        <script>postboxes.add_postbox_toggles(pagenow);</script>
        <?php
    }

    /**
     * Add meta boxes
     *
     * @return void
     */
    public function add_meta_boxes():void
    {}

    /**
     * Before render page
     *
     * @return void
     */
    public function before_render():void
    {

        /**
         * Enqueue nessecary scripts
         * 
         */
        wp_enqueue_script('postbox');
        wp_enqueue_script('admin_default');
        wp_enqueue_script('admin_sortable');

        /**
         * Add screen option
         * 
         */
        add_screen_option('layout_columns', array('max' => $this->columns, 'default' => $this->columns));

        /**
         * Add meta boxes
         * 
         */
        do_action('add_meta_boxes', $this->wp_page, null);

    }

    /**
     * Render main page
     *
     * @return void
     */
    public function render_page():void
    {
        
        /**
         * Load Template
         * 
         */
        require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'Templates' . DIRECTORY_SEPARATOR . 'custom-page-tpl.php';

    }


}

?>