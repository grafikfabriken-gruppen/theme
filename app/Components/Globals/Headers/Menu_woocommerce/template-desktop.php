<?php

/**
 * @var \GF\Components\Globals\Headers\Menu_woocommerce\Component $this
 */
?>
<div class="gf-globalnav-desktop d-none d-lg-block">
	<div class="gn-nav">
		<div class="gn-content">

			<div class="gn-logo">
				<a href="<?= get_home_url('/'); ?>">
					<?php if ($logo = $this->get_header_logo()) : ?>
					<img src="<?= $logo ?>" alt="<?= get_bloginfo('name'); ?>">
					<?php endif; ?>
				</a>
			</div><!-- .gn-logo -->

			<?= $this->main_menu_html; ?>

			<ul class="gn-buttons">
				<li class="gn-searchbar-container">
					<button class="gn-search-btn">
						<i class="far fa-search"></i>
					</button><!-- /.gn-search-btn -->
					<div class="gn-searchbar">
						<div class="gn-searchbar-inner">
							<form action="<?= home_url('/'); ?>" method="GET">
								<div class="form-group">
									<input type="search" class="form-control" name="s"
										value="<?= isset($_GET["s"]) ? $_GET["s"] : ""; ?>"
										placeholder="<?= __('Search for products, brand', 'grafikfabriken') ?>">
									<i class="far fa-search"></i>
								</div><!-- /.form-group -->
							</form>
						</div>
					</div><!-- .gn-searchbar -->
				</li><!-- .gn-searchbar-container -->

				<li class="gn-user-container">
					<button class="gn-user-btn">
						<i class="far fa-user"></i>
					</button><!-- /.gn-user-btn -->
				</li><!-- .gn-user-container -->

				<li class="gn-minicart-container">
					<button class="gn-cart-btn js-minicarttoggle">
						<i class="far fa-shopping-basket"></i>
						<?= $this->get_cart_count(null)['span.gn-cart-count']; ?>
					</button><!-- /.gn-search-btn -->
					<div class="gn-minicart">
						<div class="gn-minicart-inner">
							<div class="widget_shopping_cart_content">
							</div> <!-- /.widget_shopping_cart_content -->
						</div>
					</div><!-- .gn-minicart -->
				</li><!-- .gn-minicart-container-->
			</ul><!-- .gn-buttons -->

		</div><!-- .gn-content -->
	</div><!-- .gn-nav -->
</div><!-- .gf-globalnav-desktop -->