<?php if($data['percent']): ?>
	<div class="free-shipping-progressbar">
		<?= $data['message'] ?>
		<div class="progress <?= $data['class'] ?>">
			<div class="progress-bar" role="progressbar" aria-valuenow="<?= $data['percent'] ?>" aria-valuemin="0" aria-valuemax="100"
				style="width:<?= $data['percent'] ?>%">
				<span class="sr-only"><?= $data['percent'] ?>% Complete</span>
			</div>
		</div>
	</div><!-- .free-shipping-progressbar -->
<?php endif; ?>