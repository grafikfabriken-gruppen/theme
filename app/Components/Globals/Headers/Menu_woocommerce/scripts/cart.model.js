class Cart {
    /**
     * Get instance of cart
     *
     * @param {JQuery} root
     */
    constructor(root) {
        /**
         * Root elemen
         *
         * @type {JQuery}
         */
        this.root = root;

        /**
         * Search Button
         *
         * @type {JQuery}
         */
        this.cart_button = this.root.find('button:first-of-type').first();

        /**
         * Sub element
         *
         * @type {JQuery}
         */
        this.sub_element = this.root.find('.gn-minicart:first-of-type').first();

        /**
         * Timeout notice
         *
         * @type {Function}
         *
         */
        this.timeout_notice = null;

        /**
         * Timeout cart
         *
         * @type {Function}
         */
        this.timeout_cart = null;

        /**
         * Is open ?
         *
         * @type {boolean}
         *
         */
        this.is_open = false;

        /**
         * Ugly JS binds..
         *
         */
        this.click = this.click.bind(this);
        this.resize = this.resize.bind(this);
        this.documentClick = this.documentClick.bind(this);
        this.added_to_cart = this.added_to_cart.bind(this);

        /**
         * Height of element
         *
         * @type {int}
         */
        this.height = 0;

        /**
         * css class name
         *
         * @type {String}
         */
        this.cssClassName = 'js-cartopen';

        /**
         * Events
         */
        this.cart_button.on('click', this.click);
        $(window).on('resize', this.resize);
        $(document.body).on('added_to_cart', this.added_to_cart);
        $(document.body).on('removed_from_cart', this.resize);

        /**
         * Init events
         *
         */
        // this.resize;
        $(document.body).on('wc_fragments_refreshed wc_fragments_loaded', this.resize);
    }

    /**
     * WooCommerce Added to cart
     *
     */
    added_to_cart() {
        this.resize();

        if (!this.is_open) {
            this.show();
        }

        // if (this.timeout_cart !== null) clearTimeout(this.timeout_cart);

        // this.timeout_cart = setTimeout(() => {
        //   this.hide();
        // }, 2000);

        // // $(".cart_notice")
        // //   .stop()
        // //   .removeClass("animated")
        // //   .removeClass("slideOutRight")
        // //   .removeClass("slideInRight")
        // //   .addClass("animated slideInRight");

        // if (this.timeout_notice !== null) clearTimeout(this.timeout_notice);

        // this.timeout_notice = setTimeout(function() {
        //   $(".cart_notice")
        //     .stop()
        //     .removeClass("slideInRight")
        //     .addClass("slideOutRight");
        //   clearTimeout(this.timeout_notice);
        //   this.timeout_notice = null;
        // }, 12000);
    }

    /**
     * Resize
     *
     * @param {Event} e
     */
    resize(e) {
        this.height = this.sub_element.find('.gn-minicart-inner').first().outerHeight();
        this.set_height();
    }

    /**
     * Set height
     */
    set_height() {
        if (this.is_open) {
            this.sub_element.css({height: this.height});
        } else {
            this.sub_element.css({height: 0});
        }
    }

    /**
     * Show element
     *
     * @returns {void}
     */
    show() {
        this.sub_element.addClass(this.cssClassName);
        this.is_open = true;
        this.set_height();
        $(document).on('mouseup touchend keydown', this.documentClick);
    }

    /**
     * Hide element
     *
     * @returns {void}
     */
    hide() {
        this.sub_element.removeClass(this.cssClassName);
        this.is_open = false;
        this.set_height();
        $(document).off('mouseup touchend keydown', this.documentClick);
    }

    /**
     * Toggle element
     *
     */
    toggle() {
        if (this.is_open) this.hide();
        else this.show();
    }

    /**
     * Click
     *
     * @param {Event} e
     */
    click(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this.toggle();
    }

    /**
     * Get classnames
     * @returns {String}
     */
    getClassNames() {
        return this.root[0].className != '' ? '.' + this.root[0].className.replace(/ /g, '.') : '';
    }

    /**
     * Document click
     *
     * @param {Event} e
     */
    documentClick(e) {
        const $target = $(e.target);

        if (
            (typeof e.keyCode != 'undefined' && e.keyCode === 27) ||
            !$target.parents(this.getClassNames()).length > 0
        ) {
            this.hide();
        }
    }
}

export {Cart};
