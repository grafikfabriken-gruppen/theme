import $ from 'jquery';

class SubNavItem {
    /**
     * Drop down item
     *
     * @param {JQuery} root
     * @param {NavItem} parent
     */
    constructor(root, parent) {
        /**
         * Root element
         *
         * @type {JQuery}
         */
        this.root = root;

        /**
         * Link element
         *
         * @type {JQuery}
         */
        this.link_element = this.root.find('a:first-of-type').first();

        /**
         * Sub element
         */
        this.sub_element = this.root.find('ul.gn-second-subnav').first();

        /**
         * Parent
         */
        this.parent = parent;

        /**
         * Is open
         *
         * @type {boolean}
         */
        this.is_open = this.root.hasClass('open');

        /**
         * Height of element
         *
         * @type {int}
         */
        this.height = 0;

        /**
         * Uggly binds...
         *
         */
        this.click = this.click.bind(this);
        this.documentClick = this.documentClick.bind(this);
        this.hide = this.hide.bind(this);
        this.show = this.show.bind(this);
        this.resize = this.resize.bind(this);
        this.open_sidebar = this.open_sidebar.bind(this);

        /**
         * Events
         *
         */
        this.link_element.on('click', this.click);
        $(window).on('resize', this.resize);
        $(document).on('open_sidebar', this.open_sidebar);
        // $(window).on('scroll', this.hide);

        /**
         * Init methods
         *
         */
        this.insertBreakBeforeImageItem();
        this.resize();
    }

    /**
     * On open sidebar
     *
     * @returns {void}
     */
    open_sidebar() {
        if (this.root.find('li.active').length > 0) {
            this.show();
        }
    }

    /**
     * Insert break before
     * image item
     */
    insertBreakBeforeImageItem() {
        let $li = this.sub_element.find('li.has-image').first();
        $('<li></li>', {
            class: 'break',
        }).insertBefore($li);
    }

    /**
     * Resize
     *
     * @param {Event} e
     */
    resize(e) {
        this.sub_element.css({height: 'auto'});
        this.height = this.sub_element.outerHeight();
        this.set_height();
        this.parent.resize();
    }

    /**
     * Set height
     */
    set_height() {
        if (this.is_open) {
            this.sub_element.css({height: this.height});
        } else {
            this.sub_element.css({height: 0});
        }
    }

    /**
     * Hide menu
     *
     */
    hide() {
        this.sub_element.css({height: 0});
        this.is_open = false;
        this.root.removeClass('open');
        $(document).off('mouseup touchend keydown', this.documentClick);
        this.parent.resize();
    }

    /**
     * Show menu
     */
    show() {
        console.log(this.sub_element);
        this.sub_element.css({height: this.height});
        this.is_open = true;

        let totalW =
            this.parent.root.position().left +
            this.root.outerWidth() +
            this.sub_element.outerWidth();

        if (totalW > $(window).innerWidth()) {
            this.root.addClass('left');
        } else {
            this.root.removeClass('left');
        }

        this.root.addClass('open');
        $(document).on('mouseup touchend keydown', this.documentClick);
        this.parent.resize();
    }

    /**
     * Toggle
     *
     */
    toggle() {
        if (this.is_open) this.hide();
        else this.show();
    }

    /**
     * Click
     *
     * @param {Event} e
     */
    click(e) {
        e.preventDefault();
        e.stopPropagation();
        this.toggle();
    }

    /**
     * Get classnames
     * @returns {String}
     */
    getClassNames() {
        return this.root[0].className != '' ? '.' + this.root[0].className.replace(/ /g, '.') : '';
    }

    /**
     * Document click
     *
     * @param {Event} e
     */
    documentClick(e) {
        const $target = $(e.target);

        if (
            (typeof e.keyCode != 'undefined' && e.keyCode === 27) ||
            !$target.parents(this.getClassNames()).length > 0
        ) {
            this.hide();
        }
    }
}

export {SubNavItem};
