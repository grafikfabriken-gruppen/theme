import $ from 'jquery';
import {SubNavItem} from './sub_nav_item';

class NavItem {
    /**
     * Drop down item
     *
     * @param {JQuery} root
     */
    constructor(root) {
        /**
         * Root element
         *
         * @type {JQuery}
         */
        this.root = root;

        /**
         * Link element
         *
         * @type {JQuery}
         */
        this.link_element = this.root.find('a:first-of-type').first();

        /**
         * Sub element
         */
        this.sub_element = this.root.find('ul.gn-subnav').first();

        /**
         * Sub Nav elements
         *
         * @type {NavItem[]}
         */
        this.sub_nav_elements = root
            .find('ul.gn-subnav-list > li.gn-navlist-item-second-has-subnav')
            .toArray()
            .map(elm => new SubNavItem($(elm), this));

        /**
         * Is open
         *
         * @type {boolean}
         */
        this.is_open = this.root.hasClass('open');

        /**
         * Height of element
         *
         * @type {int}
         */
        this.height = 0;

        /**
         * Uggly binds...
         *
         */
        this.click = this.click.bind(this);
        this.documentClick = this.documentClick.bind(this);
        this.hide = this.hide.bind(this);
        this.show = this.show.bind(this);
        this.resize = this.resize.bind(this);
        this.open_sidebar = this.open_sidebar.bind(this);

        /**
         * Events
         *
         */
        this.link_element.on('click', this.click);
        $(window).on('resize', this.resize);
        $(document).on('open_sidebar', this.open_sidebar);
        // $(window).on('scroll', this.hide);

        /**
         * Init methods
         *
         */
        this.insertBreakBeforeImageItem();
        this.resize();
    }

    /**
     * On open sidebar
     *
     * @returns {void}
     */
    open_sidebar() {
        if (this.root.find('li.active').length > 0) {
            this.show();
        }
    }

    /**
     * Insert break before
     * image item
     */
    insertBreakBeforeImageItem() {
        let $li = this.sub_element.find('li.has-image').first();
        $('<li></li>', {
            class: 'break',
        }).insertBefore($li);
    }

    /**
     * Resize
     *
     * @param {Event} e
     */
    resize(e) {
        console.log(this);
        this.height = this.sub_element.find('.gn-subnav-row').first().outerHeight();

        console.log(this.height);
        this.set_height();
    }

    /**
     * Set height
     */
    set_height() {
        if (this.is_open) {
            this.sub_element.css({height: this.height});
        } else {
            this.sub_element.css({height: 0});
        }
    }

    /**
     * Hide menu
     *
     */
    hide() {
        this.sub_element.css({height: 0});
        this.is_open = false;
        this.root.removeClass('open');
        $(document).off('mouseup touchend keydown', this.documentClick);
    }

    /**
     * Show menu
     */
    show() {
        this.sub_element.css({height: this.height});
        this.is_open = true;
        this.root.addClass('open');
        $(document).on('mouseup touchend keydown', this.documentClick);
    }

    /**
     * Toggle
     *
     */
    toggle() {
        if (this.is_open) this.hide();
        else this.show();
    }

    /**
     * Click
     *
     * @param {Event} e
     */
    click(e) {
        e.preventDefault();
        e.stopPropagation();
        this.toggle();
    }

    /**
     * Get classnames
     * @returns {String}
     */
    getClassNames() {
        return this.root[0].className != '' ? '.' + this.root[0].className.replace(/ /g, '.') : '';
    }

    /**
     * Document click
     *
     * @param {Event} e
     */
    documentClick(e) {
        const $target = $(e.target);

        if (
            (typeof e.keyCode != 'undefined' && e.keyCode === 27) ||
            !$target.parents(this.getClassNames()).length > 0
        ) {
            this.hide();
        }
    }
}

export {NavItem};
