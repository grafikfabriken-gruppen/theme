<?php

/**
 * @var \GF\Components\Globals\Headers\Menu_woocommerce\Component $this
 */
?>
<div class="gf-globalnav-mobile d-lg-none gf-mobile-menu">
	<div class="gn-nav">
		<div class="gn-content">
			<ul class="gn-header-list">
				<li class="gn-header-logo">
					<a href="<?= get_home_url('/'); ?>">
						<?php if ($logo = $this->get_header_logo()) : ?>
						<img src="<?= $logo ?>" alt="<?= get_bloginfo('name'); ?>">
						<?php endif; ?>
					</a>
				</li><!-- .gn-header-logo -->
				<li class="gn-header-buttons">
					<ul>
						<li class="gn-user-container">
							<button class="gn-user-btn">
								<i class="far fa-user"></i>
							</button><!-- /.gn-user-btn -->
						</li><!-- .gn-user-container -->

						<li class="gn-minicart-container">
							<button class="gn-cart-btn js-minicarttoggle">
								<i class="far fa-shopping-basket"></i>
								<?= $this->get_cart_count(null)['span.gn-cart-count']; ?>
							</button><!-- /.gn-search-btn -->
							<div class="gn-minicart">
								<div class="gn-minicart-inner">
									<div class="widget_shopping_cart_content">
									</div> <!-- /.widget_shopping_cart_content -->
								</div>
							</div><!-- .gn-minicart -->
						</li><!-- .gn-minicart-container-->

						<li>
							<button class="gn-toggle-btn"><i class="far fa-bars"></i></button>
						</li>
					</ul>
				</li><!-- .gn-header-buttons -->
			</ul><!-- .gn-header-list -->
		</div><!-- .gn-content -->
	</div><!-- .gn-nav -->
	<nav class="gf-mobilnav">
		<div class="gn-content">
			<div class="mn-header">
				<h5 class="mn-title"><?= __('Menu', 'grafikfabriken') ?></h5><!-- .mn-title -->
				<span class="mn-closenav"><i class="far fa-times"></i></span><!-- .mn-closenav -->
			</div><!-- .mn-header -->
			<div class="gn-searchbar">
				<form action="<?= home_url('/'); ?>" method="GET">
					<div class="form-group">
						<input type="search" class="form-control" name="s"
							value="<?= isset($_GET["s"]) ? $_GET["s"] : ""; ?>"
							placeholder="<?= __('Search for products, brand', 'grafikfabriken') ?>">
						<i class="far fa-search"></i>
					</div><!-- /.form-group -->
				</form>
			</div><!-- .gn-searchbar -->
			<hr>
			<div class="scroll-container">
				<?php


				$menu = wp_nav_menu(array(
					'theme_location' => $this->menu_mobile,
					"menu_class" => "icon-container",
					"container" => "ul",
					"walker" => $this->menu_walker,
					"menu_id" => uniqid('mobile_menu_'),
					"fallback_cb" => null,
					"echo" => false
				));
				if($menu){
					echo $menu;
					echo '<hr>';
				}
				?>

				<nav class="nav-container">
					<?= $this->main_menu_html; ?>
				</nav><!-- .nav-container -->
			</div><!-- .scroll-container -->
		</div><!-- .gn-content -->
	</nav><!-- .gf-mobilnav -->
</div><!-- .gf-globalnav-mobile -->