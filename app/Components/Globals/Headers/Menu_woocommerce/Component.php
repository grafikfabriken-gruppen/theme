<?php

namespace GF\Components\Globals\Headers\Menu_woocommerce;

use App\Controllers\ACF_Controller;
use App\Controllers\Store_Controller;

use function GF\Utils\optionsPageFactory;


final class Component extends \GF\Models\Component
{

    /**
     * Post type
     *
     * @var string
     */
    public $post_type = "nav_menu_item";

    /**
     * Menu walker
     *
     * @var \GF\Components\Globals\Headers\Menu_woocommerce\Menu_Walker
     */
    public $menu_walker;

    /**
     * Menu
     *
     * @var string
     */
    public $menu = "main_menu";

    /**
     * Menu top left
     *
     * @var string
     */
    public $menu_top_left = "menu_top_left";

    /**
     * Menu top right
     *
     * @var string
     */
    public $menu_top_right = "menu_top_right";

    /**
     * Menu mobile
     *
     * @var string
     */
    public $menu_mobile = "menu_mobile";
    

    /**
     * Mini menu
     *
     * @var \GF\Components\Partials\Menu_woocommerce\Mini\Component
     */
    // public $mini_menu;

    /**
     * Array of the languages available for each page
     *
     * @var array
     */
    public $languages = array();

    /**
     * Current language
     *
     * @var array
     */
    public $current_language;

    /**
     * Has options?
     *
     * @var boolean
     */
    public $has_options = true;

    /**
     * ACF Controller
     *
     * @var \App\Controllers\ACF_Controller
     */
    public $acf_controller;

    /**
     * Store controller
     *
     * @var \App\Controllers\Store_Controller
     */
    public $store_controller;

    /**
     * Header background
     *
     * @var int
     */
    public $header_background;

    /**
     * Background image
     *
     * @var string
     */
    public $background_image;

    public $product_id = 0;

    /**
     * Cache component
     *
     * @var boolean
     */
    public $cache = true;

    /**
     * Main menu html
     *
     * @var string
     */
    public $main_menu_html = "";

    public function __construct()
    {

        /**
         * Use mini menu
         */
        // $this->partials = array(
        //     "mini_menu" => array(
        //         "folder" => "Menu",
        //         "id" => "Mini",
        //     )
        // );

        // $this->store_controller = Store_Controller::getInstance();

        $this->acf_controller = ACF_Controller::getInstance();


    }


    /**
     * Calculate the remains to free shipping
     *
     * @return array
     */
    public function get_free_shipping_data(){
        $message = null;
        $class = '';
        
        // Calculate the shipping - since it's ajax (mostly) it won't be done on init
        WC()->shipping()->calculate_shipping( WC()->cart->get_shipping_packages() );

        $methods = WC()->shipping()->get_shipping_methods();
        $packages = WC()->shipping()->get_packages();


        $return = array(
            'message' => '',
            'percent' => 0,
            'class' => ''
        );


        if(array_has_items($packages)){
            foreach($packages as $package){
                $methods = WC()->shipping()->load_shipping_methods($package);
                if(array_has_items($methods)){
                    foreach($methods as $method){
                        if(is_a($method, 'WC_Shipping_Free_Shipping') || is_a($method, 'WC_GF_Shipping_Free_Shipping') ){
                            /** @var WC_Shipping_Free_Shipping $method */
                            
                            if($method->requires === 'min_amount' || $method->requires === 'either'){
                                //dpr($method,2);    
                                $min_amount = $method->min_amount;
                                $cart_total = WC()->cart->get_subtotal() + WC()->cart->get_subtotal_tax();
                               
                                if($min_amount > 0){
            
                                    $left = $min_amount - $cart_total;

                                    $return['percent'] = min($cart_total/$min_amount*100, 100);

                                    if($left > 0){
                                        
                                        $return['message'] = sprintf(
                                            '<span>'.__('<strong>%s</strong> left to free shipping', 'grafikfabriken'). '</span>',
                                            wc_price($left)
                                        );
                                        
            
                                    } else {
                                        $return['message'] = sprintf(
                                            '<span>' . __('You\'ve reached free shipping', 'grafikfabriken'). '</span>'
                                        );
                                        $return['class'] = 'has-free-shipping';
                                    }
            
                                }
                            }
                        }
                    }
                }
            }
        }



        return $return;
    }

    /**
     * Create a global settings page
     *
     * @return void
     */
    public function theme_hooks()
    {

        /**
         * Load the fields
         */
        add_action('acf/init', array($this, 'load_fields'));

        //Add options page
        $this->fields = require('fields.php');
        optionsPageFactory()->create_theme_sub_page(__('Header', 'grafikfabriken'), 'header-settings', $this->fields);

        //Register menu
        register_nav_menus(array(
            $this->menu => __('Menu menu', 'grafikfabriken'),
            $this->menu_top_left => __('Menu top left', 'grafikfabriken'),
            $this->menu_top_right => __('Menu top right', 'grafikfabriken'),
            $this->menu_mobile => __('Menu mobile', 'grafikfabriken'),
        ));

        //Change li class
        add_filter('nav_menu_css_class', array($this, 'custom_nav_menu_css_class'), PHP_INT_MAX, 4);

        //Change a class
        add_filter('nav_menu_link_attributes', array($this, 'add_specific_menu_location_atts'), PHP_INT_MAX, 4);

        //Change submenu classes
        add_filter('nav_menu_submenu_css_class', array($this, 'custom_nav_menu_submenu_css_class'), PHP_INT_MAX, 3);

        // Add fragment to the cart
        add_filter('woocommerce_add_to_cart_fragments', array($this, 'get_cart_count'));

        add_filter('woocommerce_add_to_cart_fragments', array($this, 'display_latest_add_to_cart'));

        
        add_action('woocommerce_ajax_added_to_cart', array($this, 'set_latest_product_id'));


		add_action('woocommerce_before_mini_cart', array($this, 'render_before_cart'));
    }


    /**
     * Render the progresbar before the cart
     *
     * @return string
     */
	public function render_before_cart(){
        $data = $this->get_free_shipping_data();
		require_once 'before-cart.tpl.php';

	}

    public function set_latest_product_id($product_id){
        if($product_id > 0){
            $this->product_id = $product_id;
        }
    }

    /**
     * Get the cart count
     *
     * @global \WooCommerce $woocommerce
     * @param array|null $fragments
     * @return array|void
     */
    public function display_latest_add_to_cart($fragments)
    {
        /**
         * @var \WooCommerce $woocommerce
         */
        global $woocommerce;

        $string = '<div class="cart_notice shadow">';

        if ($this->product_id > 0 && $product = wc_get_product($this->product_id)) {
            $string .= '<p>'.__('You\'ve added', 'grafikfabriken').' <strong>' . $product->get_title() . '</strong> '. __('in the cart', 'grafikfabriken') .'.</p>';
            $string .= '<a href="'.wc_get_checkout_url().'" class="btn btn-primary btn-block">'. __('Go to the checkout', 'grafikfabriken') .'</a>';
        }

        $string .= '</div>';

        $fragments['div.cart_notice'] = $string;

        return $fragments;
    }



    /**
     * Get the cart count
     *
     * @param array|null $fragments
     * @return array|void
     */
    public function get_cart_count($fragments)
    {
        global $woocommerce;
        ob_start();

    ?>
<span class="gn-cart-count"><?= $woocommerce->cart->cart_contents_count ?></span><!-- /.gn-cart-count -->
<?php

        $fragments['span.gn-cart-count'] = ob_get_clean();
        return $fragments;
    }

    /**
     * Change link class
     *
     * @param array $classes
     * @param stdClass $item
     * @param stdClass $args
     * @param int $depth
     * @return array
     */
    public function add_specific_menu_location_atts($classes, $item, $args, $depth)
    {
        if (!isset($args->theme_location)) return $classes;

        if ($args->theme_location === $this->menu) {

            if ($depth == 1) {
                $classes["class"] = 'gn-subnav-title';
            } else {
                $classes["class"] = 'gn-navlist-link';
            }
            return $classes;
        }

        return $classes;
    }

    /**
     * Add custom css class to submenus
     *
     * @param array $classes
     * @param array $args
     * @param int $depth
     * @return array
     */
    public function custom_nav_menu_submenu_css_class($classes, $args, $depth)
    {

        switch ($depth) {
            case 0:
				$classes = array('gn-subnav');
				break;
            case 1:
                $classes = array('gn-second-subnav');
                break;
            default:
                $classes = array();
                break;
        }

        return $classes;
    }

    /**
     * Change li classes
     *
     * @param array $classes
     * @param stdClass $item
     * @param stdClass $args
     * @param int $depth
     * @return array
     */
    public function custom_nav_menu_css_class($classes, $item, $args, $depth)
    {
        if (!isset($args->theme_location)) return $classes;


        if ($args->theme_location === $this->menu_top_left || $args->theme_location === $this->menu_top_right) {
            return [];
        }

        if($args->theme_location === $this->menu_mobile){
            $classes = ['icon-link'];
            return $classes;
        }

        if ($args->theme_location === $this->menu) {

            switch ($depth) {
                case 2:
                    $classes = ['gn-subnav-item'];
                    break;
                case 0:
                    $classes = ['gn-navlist-item'];
                    break;
                default:
                    $classes = [];
                    break;
            }

            if ($item->current) {
                $classes[] = 'active';
                $args->walker->active = true;
            }

            if ($args->walker->has_children && $depth == 0) {
                $classes[] = 'gn-navlist-item-has-subnav';

                $active_classes = ['current-menu-parent', 'current-menu-ancestor'];

                foreach ($item->classes as $c) {
                    if (in_array($c, $active_classes)) {
                        $classes[] = 'active';
                        break;
                    }
                }
            }

            if ($args->walker->has_children && $depth == 1) {
                $classes[] = 'gn-navlist-item-second-has-subnav';

                $active_classes = ['current-menu-parent', 'current-menu-ancestor'];

                foreach ($item->classes as $c) {
                    if (in_array($c, $active_classes)) {
                        $classes[] = 'active';
                        break;
                    }
                }
            }

            return $classes;
        }

        return $classes;
    }

    /**
     * Get the header logo url
     *
     * @return string
     */
    public function get_header_logo()
    {
        $header_logo = get_field("header_logo", "options");
        return $header_logo ? wp_get_attachment_image_url($header_logo, 'full') : "";
    }

    /**
     * Before render setup walker
     *
     * @return void
     */
    public function before_render()
    {

        $this->menu_walker = new Menu_Walker();


        $this->main_menu_html = wp_nav_menu(array(
            'menu_position' => $this->menu,
            'theme_location' => $this->menu,
            "menu_class" => "gn-navlist",
            "container" => "ul",
            "walker" => $this->menu_walker,
            "echo" => false,
            "menu_id" => uniqid('main_menu_')
        ));
    }


    /**
     * Load fields for the menu
     *
     * @return void
     */
    public function load_fields(): void
    {

        $acf_key = "general_menu_icons";
        // $fields = $this->acf_controller->get_acf_tab($acf_key, __('General', "grafikfabriken"), $this->post_type);
        $fields = require dirname(__FILE__, 1) . DIRECTORY_SEPARATOR . 'menu_fields.php';
        $this->acf_controller->add_local_field_group($fields, $acf_key, __('General', 'grafikfabriken'), 'all', $this->post_type);
    }
}