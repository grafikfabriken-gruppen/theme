<?php
return array(
    array(
        'key' => 'menu_icon',
        'label' => 'Icon',
        'name' => 'icon',
        'type' => 'font-awesome',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
        ),
        'icon_sets' => array(
            0 => 'fas',
            1 => 'far',
        ),
        'custom_icon_set' => '',
        'default_label' => '',
        'default_value' => '',
        'save_format' => 'class',
        'allow_null' => 1,
        'show_preview' => 1,
        'enqueue_fa' => 0,
        'fa_live_preview' => '',
        'choices' => array(),
    ),
    
);