import $ from 'jquery';

class Sidebar {
    /**
     * Drop down item
     *
     * @param {JQuery} root
     */
    constructor(root, button) {
        /**
         * Root element
         *
         * @type {JQuery}
         */
        this.root = root;

        /**
         * Button
         *
         * @type {JQuery}
         */
        this.button = button;

        /**
         * Close button
         *
         * @type {JQuery}
         */
        this.close_button = this.root.find('.mn-closenav');

        /**
         * Sub element
         *
         * @type {JQuery}
         */
        this.sub_element = this.root.find('.gn-content:first-of-type').first();

        /**
         * Is open
         *
         * @type {boolean}
         */
        this.is_open = this.root.hasClass('open');

        /**
         * Height of element
         *
         * @type {int}
         */
        this.height = 0;

        /**
         * Uggly binds...
         *
         */
        this.click = this.click.bind(this);
        this.documentClick = this.documentClick.bind(this);
        this.hide = this.hide.bind(this);
        this.show = this.show.bind(this);

        /**
         * Events
         *
         */
        this.button.on('click', this.click);
        this.close_button.on('click', this.hide);
        // $(window).on('scroll', this.hide);

        /**
         * Init methods
         *
         */

        this.hide();
    }

    /**
     * Hide menu
     *
     */
    hide() {
        // this.root.css({ right: -this.root.outerWidth() });
        // this.sub_element.css({ height: 0 });
        this.is_open = false;

        $('html').removeClass('no-scroll');
        $('.gf-globalnav-mobile').removeClass('open');
        this.root.removeClass('open');
        $(document).off('mouseup touchend keydown', this.documentClick);
    }

    /**
     * Show menu
     */
    show() {
        console.log('Swho sidebar');

        $(document).trigger('open_sidebar');

        // this.root.css({ right: 0 });
        // this.sub_element.css({ height: this.height });
        this.is_open = true;

        $('html').addClass('no-scroll');
        $('.gf-globalnav-mobile').addClass('open');
        this.root.addClass('open');
        $(document).on('mouseup touchend keydown', this.documentClick);
    }

    /**
     * Toggle
     *
     */
    toggle() {
        if (this.is_open) this.hide();
        else this.show();
    }

    /**
     * Click
     *
     * @param {Event} e
     */
    click(e) {
        e.preventDefault();
        e.stopPropagation();
        this.toggle();
    }

    /**
     * Get classnames
     * @returns {String}
     */
    getClassNames() {
        return this.root[0].className != '' ? '.' + this.root[0].className.replace(/ /g, '.') : '';
    }

    /**
     * Document click
     *
     * @param {Event} e
     */
    documentClick(e) {
        const $target = $(e.target);

        if (
            (typeof e.keyCode != 'undefined' && e.keyCode === 27) ||
            !$target.parents(this.getClassNames()).length > 0
        ) {
            this.hide();
        }
    }
}

export {Sidebar};
