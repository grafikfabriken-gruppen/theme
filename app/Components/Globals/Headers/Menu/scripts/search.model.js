class Search {
    /**
     * Get instance of cart
     *
     * @param {JQuery} root
     */
    constructor(root) {
        /**
         * Root elemen
         *
         * @type {JQuery}
         */
        this.root = root;

        /**
         * Search Button
         *
         * @type {JQuery}
         */
        this.search_button = this.root.find('button:first-of-type').first();

        /**
         * Sub element
         *
         * @type {JQuery}
         */
        this.sub_element = this.root.find('.gn-searchbar:first-of-type').first();

        /**
         * Close btn
         *
         * @type {JQuery}
         */
        this.close_btn = this.sub_element.find('i').first();

        /**
         * Is open ?
         *
         * @type {boolean}
         *
         */
        this.is_open = false;

        /**
         * Ugly JS binds..
         *
         */
        this.click = this.click.bind(this);
        this.hide = this.hide.bind(this);
        this.resize = this.resize.bind(this);
        this.documentClick = this.documentClick.bind(this);

        /**
         * Height of element
         *
         * @type {int}
         */
        this.height = 0;

        /**
         * css class name
         *
         * @type {String}
         */
        this.cssClassName = 'js-searchopen';

        /**
         * Events
         */
        this.search_button.on('click', this.click);
        this.close_btn.on('click', this.hide);
        $(window).on('resize', this.resize);

        /**
         * Init events
         *
         */
        this.sub_element.css({height: this.height});
        this.resize();
    }

    /**
     * Resize
     *
     * @param {Event} e
     */
    resize(e) {
        this.height = this.sub_element.find('.gn-searchbar-inner').first().outerHeight();
        this.set_height();
    }

    /**
     * Set height
     */
    set_height() {
        if (this.is_open) {
            this.sub_element.css({height: this.height});
        } else {
            this.sub_element.css({height: 0});
        }
    }

    /**
     * Show element
     *
     * @returns {void}
     */
    show() {
        this.sub_element.css({height: this.height});
        this.sub_element.addClass(this.cssClassName);
        this.is_open = true;
        this.root.find('input').focus();
        $(document).on('mouseup touchend keydown', this.documentClick);
    }

    /**
     * Hide element
     *
     * @returns {void}
     */
    hide() {
        this.sub_element.css({height: 0});
        this.root.find('input').blur();
        this.sub_element.removeClass(this.cssClassName);
        this.is_open = false;
        $(document).off('mouseup touchend keydown', this.documentClick);
    }

    /**
     * Toggle element
     *
     */
    toggle() {
        if (this.is_open) this.hide();
        else this.show();
    }

    /**
     * Click
     *
     * @param {Event} e
     */
    click(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this.toggle();
    }

    /**
     * Get classnames
     * @returns {String}
     */
    getClassNames() {
        return this.root[0].className != '' ? '.' + this.root[0].className.replace(/ /g, '.') : '';
    }

    /**
     * Document click
     *
     * @param {Event} e
     */
    documentClick(e) {
        const $target = $(e.target);

        if (
            (typeof e.keyCode != 'undefined' && e.keyCode === 27) ||
            !$target.parents(this.getClassNames()).length > 0
        ) {
            this.hide();
        }
    }
}

export {Search};
