<?php

/**
 * @var \GF\Components\Globals\Headers\Menu\Component $this
 */
?>
<div class="gf-globalnav-mobile d-lg-none gf-mobile-menu">
	<div class="gn-nav">
		<div class="gn-content">
			<ul class="gn-header-list">
				<li class="gn-header-logo">
					<a href="<?= get_home_url('/'); ?>">
						<?php if ($logo = $this->get_header_logo()) : ?>
						<img src="<?= $logo ?>" alt="<?= get_bloginfo('name'); ?>">
						<?php endif; ?>
					</a>
				</li><!-- .gn-header-logo -->
				<li class="gn-header-buttons">
					<ul class="gn-navlist">
						<li><button class="gn-toggle-btn"><i class="far fa-bars"></i></button></li>
					</ul>
				</li><!-- .gn-header-buttons -->
			</ul><!-- .gn-header-list -->
		</div><!-- .gn-content -->
	</div><!-- .gn-nav -->
	<nav class="gf-mobilnav">
		<div class="gn-content">
			<div class="mn-header">
				<h5 class="mn-title">Meny</h5><!-- .mn-title -->
				<span class="mn-closenav"><i class="far fa-times"></i></span><!-- .mn-closenav -->
			</div><!-- .mn-header -->
			<div class="gn-searchbar">
				<form action="<?= home_url('/'); ?>" method="GET">
					<div class="form-group">
						<input type="search" class="form-control" name="s"
							value="<?= isset($_GET["s"]) ? $_GET["s"] : ""; ?>"
							placeholder="<?= __('Search for products, brand', 'grafikfabriken') ?>">
						<i class="far fa-search"></i>
					</div><!-- /.form-group -->
				</form>
			</div><!-- .gn-searchbar -->
			<hr>
			<div class="scroll-container">
				<?php


				$menu = wp_nav_menu(array(
					'theme_location' => $this->menu_mobile,
					"menu_class" => "icon-container",
					"container" => "ul",
					"walker" => $this->menu_walker,
					"menu_id" => uniqid('mobile_menu_'),
					"fallback_cb" => null,
					"echo" => false
				));
				if($menu){
					echo $menu;
					echo '<hr>';
				}
				?>

				<nav class="nav-container">
					<?= $this->main_menu_html; ?>
				</nav><!-- .nav-container -->
			</div><!-- .scroll-container -->
		</div><!-- .gn-content -->
	</nav><!-- .gf-mobilnav -->
</div><!-- .gf-globalnav-mobile -->