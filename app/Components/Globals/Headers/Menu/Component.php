<?php

namespace GF\Components\Globals\Headers\Menu;

use App\Controllers\ACF_Controller;
use App\Controllers\Store_Controller;

use function GF\Utils\optionsPageFactory;


final class Component extends \GF\Models\Component
{

    /**
     * Post type
     *
     * @var string
     */
    public $post_type = "nav_menu_item";

    /**
     * Menu walker
     *
     * @var \GF\Components\Globals\Headers\Menu\Menu_Walker
     */
    public $menu_walker;

    /**
     * Menu
     *
     * @var string
     */
    public $menu = "main_menu";



    /**
     * Menu mobile
     *
     * @var string
     */
    public $menu_mobile = "menu_mobile";
    

    /**
     * Mini menu
     *
     * @var \GF\Components\Partials\Menu\Mini\Component
     */
    // public $mini_menu;

    /**
     * Array of the languages available for each page
     *
     * @var array
     */
    public $languages = array();

    /**
     * Current language
     *
     * @var array
     */
    public $current_language;

    /**
     * Has options?
     *
     * @var boolean
     */
    public $has_options = true;

    /**
     * ACF Controller
     *
     * @var \App\Controllers\ACF_Controller
     */
    public $acf_controller;

    /**
     * Header background
     *
     * @var int
     */
    public $header_background;

    /**
     * Background image
     *
     * @var string
     */
    public $background_image;

    /**
     * Cache component
     *
     * @var boolean
     */
    public $cache = true;

    /**
     * Main menu html
     *
     * @var string
     */
    public $main_menu_html = "";

    public function __construct()
    {
        $this->acf_controller = ACF_Controller::getInstance();
    }


    /**
     * Create a global settings page
     *
     * @return void
     */
    public function theme_hooks()
    {

        /**
         * Load the fields
         */
        add_action('acf/init', array($this, 'load_fields'));

        //Add options page
        $this->fields = require('fields.php');
        optionsPageFactory()->create_theme_sub_page(__('Header', 'grafikfabriken'), 'header-settings', $this->fields);

        //Register menu
        register_nav_menus(array(
            $this->menu => __('Menu menu', 'grafikfabriken'),
            $this->menu_mobile => __('Menu mobile', 'grafikfabriken'),
        ));

        //Change li class
        add_filter('nav_menu_css_class', array($this, 'custom_nav_menu_css_class'), PHP_INT_MAX, 4);

        //Change a class
        add_filter('nav_menu_link_attributes', array($this, 'add_specific_menu_location_atts'), PHP_INT_MAX, 4);

        //Change submenu classes
        add_filter('nav_menu_submenu_css_class', array($this, 'custom_nav_menu_submenu_css_class'), PHP_INT_MAX, 3);
    }


    /**
     * Change link class
     *
     * @param array $classes
     * @param stdClass $item
     * @param stdClass $args
     * @param int $depth
     * @return array
     */
    public function add_specific_menu_location_atts($classes, $item, $args, $depth)
    {
        if (!isset($args->theme_location)) return $classes;

        if ($args->theme_location === $this->menu) {

            if ($depth == 1) {
                $classes["class"] = 'gn-subnav-title';
            } else {
                $classes["class"] = 'gn-navlist-link';
            }
            return $classes;
        }

        return $classes;
    }

    /**
     * Add custom css class to submenus
     *
     * @param array $classes
     * @param array $args
     * @param int $depth
     * @return array
     */
    public function custom_nav_menu_submenu_css_class($classes, $args, $depth)
    {

        switch ($depth) {
            case 0:
				$classes = array('gn-subnav');
				break;
            case 1:
                $classes = array('gn-second-subnav');
                break;
            default:
                $classes = array();
                break;
        }

        return $classes;
    }

    /**
     * Change li classes
     *
     * @param array $classes
     * @param stdClass $item
     * @param stdClass $args
     * @param int $depth
     * @return array
     */
    public function custom_nav_menu_css_class($classes, $item, $args, $depth)
    {
        if (!isset($args->theme_location)) return $classes;

        if($args->theme_location === $this->menu_mobile){
            $classes = ['icon-link'];
            return $classes;
        }

        if ($args->theme_location === $this->menu) {

            switch ($depth) {
                case 2:
                    $classes = ['gn-subnav-item'];
                    break;
                case 0:
                    $classes = ['gn-navlist-item'];
                    break;
                default:
                    $classes = [];
                    break;
            }

            if ($item->current) {
                $classes[] = 'active';
                $args->walker->active = true;
            }

            if ($args->walker->has_children && $depth == 0) {
                $classes[] = 'gn-navlist-item-has-subnav';

                $active_classes = ['current-menu-parent', 'current-menu-ancestor'];

                foreach ($item->classes as $c) {
                    if (in_array($c, $active_classes)) {
                        $classes[] = 'active';
                        break;
                    }
                }
            }

            if ($args->walker->has_children && $depth == 1) {
                $classes[] = 'gn-navlist-item-second-has-subnav';

                $active_classes = ['current-menu-parent', 'current-menu-ancestor'];

                foreach ($item->classes as $c) {
                    if (in_array($c, $active_classes)) {
                        $classes[] = 'active';
                        break;
                    }
                }
            }

            return $classes;
        }

        return $classes;
    }

    /**
     * Get the header logo url
     *
     * @return string
     */
    public function get_header_logo()
    {
        $header_logo = get_field("header_logo", "options");
        return $header_logo ? wp_get_attachment_image_url($header_logo, 'full') : "";
    }

    /**
     * Before render setup walker
     *
     * @return void
     */
    public function before_render()
    {

        $this->menu_walker = new Menu_Walker();


        $this->main_menu_html = wp_nav_menu(array(
            'menu_position' => $this->menu,
            'theme_location' => $this->menu,
            "menu_class" => "gn-navlist",
            "container" => "ul",
            "walker" => $this->menu_walker,
            "echo" => false,
            "menu_id" => uniqid('main_menu_')
        ));
    }


     /**
     * Load fields
     *
     * @return void
     */
    public function load_fields(): void
    {

        $acf_key = "general_menu_icons";
        // $fields = $this->acf_controller->get_acf_tab($acf_key, __('General', "grafikfabriken"), $this->post_type);
        $fields = require dirname(__FILE__, 1) . DIRECTORY_SEPARATOR . 'menu_fields.php';
        $this->acf_controller->add_local_field_group($fields, $acf_key, __('General', 'grafikfabriken'), 'all', $this->post_type);
    }
}