import $ from 'jquery';
import {NavItem} from './scripts/nav_item.model';
import {Cart} from './scripts/cart.model';
import {Search} from './scripts/search.model';
import {Sidebar} from './scripts/sidebar';

class GlobalNavDesktop {
    /**
     * Returns instance on global
     * nav desktop
     *
     * @param {JQuery} root
     * @param {object} args
     */
    constructor(root, args = {}) {
        /**
         * Root element
         *
         * @type {JQuery}
         */
        this.root = root;

        /**
         * Window
         *
         * @type {JQuery}
         *
         */
        this.$window = $(window);

        /**
         * Is mobile menu
         *
         * @type {boolean}
         */
        this.is_mobile_menu = this.root.hasClass('gf-mobile-menu');

        /**
         * Topnav
         *
         * @type {JQuery}
         */
        this.top_nav =
            this.root.find('.gn-topnav').length > 0 ? this.root.find('.gn-topnav').eq(0) : null;

        /**
         * Main nav
         *
         * @type {JQuery}
         *
         */
        this.main_nav = this.root.find('.gn-nav').eq(0);

        /**
         * Admin bar
         *
         * @type {JQuery}
         *
         */
        this.admin_bar = $('body').find('#wpadminbar');

        /**
         * Nav elements
         *
         * @type {NavItem[]}
         */
        this.nav_elements = root
            .find('ul.gn-navlist > li.gn-navlist-item-has-subnav')
            .toArray()
            .map(elm => new NavItem($(elm)));

        /**
         * Cart
         *
         * @type {Cart}
         */
        this.cart = null;

        /**
         * Search
         *
         * @type {Search}
         */
        this.search = null;

        /**
         * Sidebar
         *
         * @type {Sidebar}
         *
         */
        this.sidebar = null;

        /**
         * Current scroll pos
         *
         * @type {int}
         */
        this.current_scroll_pos = 0;

        /**
         * Prev scroll pos
         *
         * @type {int}
         *
         */
        this.prev_scroll_pos = 0;

        /**
         * Scroll dir
         */
        this.scroll_dir = 'DOWN';

        /**
         * Scroll dir count px
         */
        this.scroll_count_px = 0;

        /**
         * Dummy element
         *
         * @type {JQuery}
         */
        this.dummy = $('<div/>', {
            class: 'gn-dummy',
            css: {
                height: this.main_nav.outerHeight(),
                visiblity: 'hidden',
            },
        });

        /**
         * Append dummy to root
         */
        // if (!this.is_mobile_menu) {
        //     this.dummy.appendTo(this.root);
        // }
        this.dummy.appendTo(this.root);

        /**
         * Maybee bind sidebar
         *
         */
        if (root.find('.gf-mobilnav').length > 0 && root.find('.gn-toggle-btn').length > 0) {
            this.sidebar = new Sidebar(
                root.find('.gf-mobilnav').eq(0),
                root.find('.gn-toggle-btn').eq(0)
            );
        }

        /**
         * Maybee bind cart
         *
         */
        if (root.find('.gn-minicart-container').length > 0) {
            this.cart = new Cart(root.find('.gn-minicart-container').eq(0));
        }

        /**
         * Maybee bind search
         */
        if (root.find('.gn-searchbar-container').length > 0) {
            this.search = new Search(root.find('.gn-searchbar-container').eq(0));
        }

        this.scrollTimeoutFN = null;

        /**
         * Uggly binds
         *
         *
         */
        this.push_down_content = this.push_down_content.bind(this);
        this.resize = this.resize.bind(this);
        this.scroll = this.scroll.bind(this);
        this.show = this.show.bind(this);
        this.scrollTimeout = this.scrollTimeout.bind(this);

        /**
         * Init hooks
         */
        this.push_down_content();

        /**
         * Event listeners
         *
         */
        $(window).on('resize', this.resize);
        $(window).on('scroll resize', this.scroll);
        // $(document.body).on("added_to_cart", this.show);
        this.resize();
        this.scroll();
        this.maybe_add_transparent();
    }

    /**
     * Maybee add transparent
     *
     * @returns {void}
     */
    maybe_add_transparent() {
        let first_section = $('body').find('section:first');

        if (first_section.length > 0 && first_section.hasClass('behind_header')) {
            this.root.addClass('transparent');
            $('body').addClass('menu_absolute');

            if (first_section.hasClass('dark')) {
                $('body').addClass('dark');
            } else {
                $('body').addClass('light');
            }
        }
    }

    /**
     * Show
     *
     * @returns {void}
     */
    show() {
        this.set_top_for_admin_bar_posistion();
    }

    /**
     * Hide
     *
     * @returns {void}
     */
    hide() {
        let top = -this.root.outerHeight();

        // this.main_nav.css('transform', `translateY(${top}px)`)
        this.main_nav.css('top', `${top}px`);
        // this.main_nav.addClass('translate')
        if (this.cart) {
            this.cart.hide();
        }
    }

    /**
     * Hide nav bar on scroll
     *
     * @returns {void}
     */
    hide_nav_on_scroll() {
        if (
            this.current_scroll_pos <= 0 ||
            this.is_menu_items_open() ||
            (this.cart && this.cart.is_open) ||
            (this.search && this.search.is_open) ||
            (this.sidebar && this.sidebar.is_open)
        )
            return;

        // console.log(this.current_scroll_pos, this.prev_scroll_pos);
        if (this.current_scroll_pos > this.prev_scroll_pos) {
            if (this.scroll_dir == 'DOWN') {
                this.scroll_count_px += this.current_scroll_pos - this.prev_scroll_pos;
            } else {
                this.scroll_dir = 'DOWN';
                this.scroll_count_px = this.current_scroll_pos - this.prev_scroll_pos;
            }
        } else {
            if (this.scroll_dir == 'UP') {
                this.scroll_count_px += this.prev_scroll_pos - this.current_scroll_pos;
            } else {
                this.scroll_dir = 'UP';
                this.scroll_count_px = this.prev_scroll_pos - this.current_scroll_pos;
            }
        }

        // console.log(this.scroll_dir);
        // console.log(this.scroll_count_px);

        if (this.current_scroll_pos > this.root.outerHeight()) {
            if (
                this.prev_scroll_pos > this.current_scroll_pos &&
                (this.scroll_count_px > 30 || this.current_scroll_pos < 100)
            ) {
                this.show();
            } else {
                this.hide();
            }

            this.prev_scroll_pos = this.current_scroll_pos >= 0 ? this.current_scroll_pos : 0;
        }
    }

    /**
     * Window scroll
     *
     * @param {Event} e
     */
    scroll(e) {
        this.current_scroll_pos = this.$window.scrollTop();

        //Don't do this on hidden elements
        if (this.root.css('display') == 'none') return;

        let top_nav_top = this.top_nav ? this.top_nav.position().top : 0;
        let top_nav_height = this.top_nav ? this.top_nav.outerHeight() : 0;
        let top_nav_sum = top_nav_top + top_nav_height;

        if (this.current_scroll_pos > top_nav_sum) {
            this.hide_nav_on_scroll();
            this.dummy.show();
            this.main_nav.addClass('fixed');
        } else {
            this.show();
            this.dummy.hide();
            this.main_nav.removeClass('fixed');
        }
    }

    scrollTimeout() {
        this.scrollTimeoutFN = null;
    }

    /**
     * Resize event
     *
     * @param {Event} e
     * @type {void}
     *
     */
    resize(e) {
        //Don't do this on hidden elements
        if (this.root.css('display') == 'none') return;

        this.scroll();
        this.push_down_content();
    }

    /**
     * Push down content
     *
     * @returns {void}
     */
    push_down_content() {
        this.dummy.css('height', this.main_nav.outerHeight());

        if (!this.is_menu_items_open()) {
            this.root.addClass('notransition'); // Disable transitions
            // $("body").css("padding-top", this.root.outerHeight() + "px");
            this.root[0].offsetHeight; // Trigger a reflow, flushing the CSS changes
            this.root.removeClass('notransition'); // Re-enable transitions
        }

        if (this.has_admin_bar()) {
            this.root.addClass('notransition'); // Disable transitions
            this.set_top_for_admin_bar_posistion();
            this.root[0].offsetHeight; // Trigger a reflow, flushing the CSS changes
            this.root.removeClass('notransition'); // Re-enable transitions
        }
    }

    /**
     * Set top for admin bar position
     *
     * @returns {void}
     */
    set_top_for_admin_bar_posistion() {
        let top = this.admin_bar.css('position') == 'absolute' ? 0 : this.get_admin_bar_height();

        this.main_nav.css({
            top: top,
        });
        // this.main_nav.css({
        //     top: 0,
        //     // top:
        //     //     this.admin_bar.css('position') == 'absolute'
        //     //         ? '0px'
        //     //         : this.get_admin_bar_height() + 'px',
        //     transform: `translateY(${top}px)`,
        // })
        // this.main_nav.removeClass('translate')
        if (this.sidebar) {
            this.sidebar.root.css({
                top:
                    this.admin_bar.css('position') == 'absolute'
                        ? '0'
                        : this.get_admin_bar_height() + 'px',
            });
        }
    }

    /**
     * Get admin bar height
     *
     * @returns {int}
     */
    get_admin_bar_height() {
        return this.admin_bar.length > 0 ? this.admin_bar.outerHeight() : 0;
    }

    /**
     * Is menu items open
     *
     * @returns {boolean}
     */
    is_menu_items_open() {
        return (
            this.nav_elements.length > 0 &&
            this.nav_elements.filter(item => item.is_open).length > 0
        );
    }

    /**
     * Has adminbar
     *
     * @returns {boolean}
     */
    has_admin_bar() {
        return $('body').hasClass('admin-bar');
    }

    static instance(args = {}) {
        if ($(this).length > 0) {
            $(this).each((index, elm) => {
                new GlobalNavDesktop($(elm));
            });
        }
    }
}

$.fn.mainMenu = GlobalNavDesktop.instance;

$(_ => {
    $('.gf-globalnav-desktop,.gf-globalnav-mobile').mainMenu();
});
