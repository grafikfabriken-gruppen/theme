<?php /** @var \GF\Components\Blocks\Outdatedbrowsers\v1\Component $block */ ?>


<div id="outdated">
	<div class="content">
		<?php if ($logo = $this->get_logo()) : ?>
		<img src="<?= $logo ?>" alt="<?= get_bloginfo('name'); ?>">
		<?php endif; ?>

		<h1><?= __('You\'re using an old browser', 'grafikfabriken') ?></h1>
		<p>
			<?= __('Old browsers only have partial support for new technology and doesn\'t give the website an honset chance to live up to its fully potential. You can easily download an other web browser that works better.', 'grafikfabriken') ?>
			<?= sprintf(__('%sHere%s is only a few modern browsers that works great!', 'grafikfabriken'), '<a href="https://browsehappy.com/">', '</a>' ) ?>
		</p>
		<p>
			<a href="https://browsehappy.com/" target="_blank"
				class="btn btn-primary"><?= __('Update my browser', 'grafikfabriken') ?></a>
		</p>
		<p class="last">
			<a href="#" id="btnCloseUpdateBrowser"
				title="<?= __('Close', 'grafikfabriken') ?>"><?= __('I\'d like to continue anyway', 'grafikfabriken') ?></a>
		</p>
	</div>
</div>