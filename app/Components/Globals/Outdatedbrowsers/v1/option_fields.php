<?php
return [
    array(
        'key' => 'outdated_browser_logo',
        'label' => __('Logotype', 'imrab'),
        'name' => 'outdated_browser_logo',
        "class_key" => "outdated_browser_logo",
        'type' => 'image',
        'instructions' => __('Enter a logo version that would go well on a light color', 'grafikfabriken'),
        'return_format' => 'id',
        'preview_size' => 'thumbnail',
    ),
];
