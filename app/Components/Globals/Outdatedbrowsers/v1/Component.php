<?php
namespace GF\Components\Globals\Outdatedbrowsers\v1;

use function GF\Utils\optionsPageFactory;

final class Component extends \GF\Models\Component{

    public $fields;

    /**
     * Logo id
     *
     * @var int
     */
    public $outdated_browser_logo;

    public $options_in_use = [
        'outdated_browser_logo'
    ];

    public function theme_hooks()
    {
         //Add options page
         optionsPageFactory()->create_theme_sub_page(__('Outdated browser', 'grafikfabriken'), 'outdated-browser-settings', $this->option_fields);    
    }

    /**
     * Get the logo
     *
     * @return string
     */
    public function get_logo():string
    {

       if($this->outdated_browser_logo < 1) return '';

       $url = wp_get_attachment_image_url($this->outdated_browser_logo, 'full');
       return $url ? (strpos($url, '.svg') === false ? aq_resize($url, 300) : $url) : '';
   }
}