/*!--------------------------------------------------------------------
JAVASCRIPT "Outdated Browser"
Version:    1.1.2 - 2015
author:     Burocratik
website:    http://www.burocratik.com
* @preserve
-----------------------------------------------------------------------*/
class outdatedBrowser {
    constructor(options) {
        var s = this;
        var bkgColor;

        //Variable definition (before ajax)
        s.outdated = document.getElementById('outdated');

        var cssProp = {};

        if (options) {
            //assign css3 property or js property to IE browser version
            if (options.lowerThan == 'IE8' || options.lowerThan == 'borderSpacing') {
                options.lowerThan = 'borderSpacing';
            } else if (options.lowerThan == 'IE9' || options.lowerThan == 'boxShadow') {
                options.lowerThan = 'boxShadow';
            } else if (
                options.lowerThan == 'IE10' ||
                options.lowerThan == 'transform' ||
                options.lowerThan == '' ||
                typeof options.lowerThan === 'undefined'
            ) {
                options.lowerThan = 'transform';
            } else if (options.lowerThan == 'IE11' || options.lowerThan == 'borderImage') {
                options.lowerThan = 'borderImage';
            } else if (options.lowerThan == 'Edge' || options.lowerThan == 'js:Promise') {
                options.lowerThan = 'js:Promise';
            }

            cssProp = options.lowerThan;
        } else {
            cssProp = 'transform';
        }

        //Define opacity and fadeIn/fadeOut functions
        var done = true;

        function function_opacity(opacity_value) {
            s.outdated.style.opacity = opacity_value / 100;
            s.outdated.style.filter = 'alpha(opacity=' + opacity_value + ')';
        }

        // function function_fade_out(opacity_value) {
        //     function_opacity(opacity_value);
        //     if (opacity_value == 1) {
        //         outdated.style.display = 'none';
        //         done = true;
        //     }
        // }

        function get_browser() {
            var ua = navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return {name: 'IE', version: tem[1] || ''};
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\bOPR|Edge\/(\d+)/);
                if (tem != null) {
                    return {name: 'Opera', version: tem[1]};
                }
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) != null) {
                M.splice(1, 1, tem[1]);
            }
            return {
                name: M[0],
                version: M[1],
            };
        }

        function function_fade_in(opacity_value) {
            function_opacity(opacity_value);
            if (opacity_value == 1) {
                s.outdated.style.display = 'block';
            }
            if (opacity_value == 100) {
                done = true;
            }
        }

        function cssPropertyValueSupported(prop, value) {
            var d = document.createElement('div');
            d.style[prop] = value;
            return d.style[prop] === value;
        }

        var validBrowser = true;

        // browser check by js props
        if (/^js:+/g.test(cssProp)) {
            console.log('prop');

            var jsProp = cssProp.split(':')[1];
            if (!jsProp) return;

            switch (jsProp) {
                case 'Promise':
                    validBrowser =
                        window.Promise !== undefined &&
                        window.Promise !== null &&
                        Object.prototype.toString.call(window.Promise.resolve()) ===
                            '[object Promise]';
                    break;
                default:
                    validBrowser = false;
            }
        } else {
            // validBrowser = supports('' + cssProp + '');
            validBrowser = cssPropertyValueSupported('filter', 'blur(5px)');
        }

        var browser = get_browser();

        if (browser.name == 'Safari' && parseInt(browser.version) < 9) {
            validBrowser = false;
        }

        // Set to false in order to debug the outdated-browser-screen
        // validBrowser = false;

        if (!validBrowser) {
            if (done && s.outdated.style.opacity !== '1') {
                document.body.className += ' ' + 'invalid_browser';
                done = false;
                for (var i = 1; i <= 100; i++) {
                    setTimeout(
                        (function (x) {
                            return function () {
                                function_fade_in(x);
                            };
                        })(i),
                        i * 8
                    );
                }
            }
        } else {
            return;
        } //end if

        //Check AJAX Options: if languagePath == '' > use no Ajax way, html is needed inside <div id="outdated">
        startStylesAndEvents();

        //events and colors
        function startStylesAndEvents() {
            var btnClose = document.getElementById('btnCloseUpdateBrowser');

            //close button
            btnClose.onmousedown = function () {
                s.outdated.style.display = 'none';
                s.outdated.className += ' closed';
                document.body.className = document.body.className.replace('invalid_browser', '');

                var cookie = new gfCookie();
                cookie.set('outdated_browser', 'yes', {path: '/'});
                return false;
            };
        } //end styles and events

        ////////END of outdatedBrowser function
    }
}

//Grafikfabriken Addon
class gfOutdatedBrowser {
    constructor() {
        console.log(this);

        this.cookie_name = 'outdated_browser';
        this.cookie = new gfCookie();

        this.onload();
    }
    onload(func) {
        var oldonload = window.onload;

        if (typeof window.onload != 'function') {
            window.onload = func;
        } else {
            window.onload = function () {
                if (oldonload) {
                    oldonload();
                }
                func();
            };
        }

        var cookie = this.cookie.get(this.cookie_name);

        if (cookie === '') {
            $(document).on('ready', () => {
                new outdatedBrowser({
                    bgColor: '#B60014',
                    color: '#ffffff',
                    lowerThan: 'Edge',
                    languagePath: ' ',
                });
            });
        }
    }
}

class gfCookie {
    /**
     * Get cookie
     */
    get(sName) {
        var oCrumbles = document.cookie.split(';');
        for (var i = 0; i < oCrumbles.length; i++) {
            var oPair = oCrumbles[i].split('=');
            var sKey = decodeURIComponent(oPair[0].trim());
            var sValue = oPair.length > 1 ? oPair[1] : '';
            if (sKey == sName) {
                return decodeURIComponent(sValue);
            }
        }
        return '';
    }

    /**
     * Set cookie
     */
    set(sName, sValue, options) {
        //oDate.setYear(oDate.getFullYear()+1);
        var sCookie = encodeURIComponent(sName) + '=' + encodeURIComponent(sValue);

        // Shorthand: options === expires date
        if (options && options instanceof Date) {
            options = {
                expires: options,
            };
        }
        // Longhand: options object
        if (options && typeof options == 'object') {
            if (options.expires) {
                sCookie += '; expires=' + options.expires.toGMTString();
            }
            if (options.path) {
                sCookie += '; path=' + options.path.toString();
            }
            if (options.domain) {
                sCookie += '; domain=' + options.domain.toString();
            }
            if (options.secure) {
                sCookie += '; secure';
            }
        }
        document.cookie = sCookie;
    }

    /**
     * Remove cookie
     */
    remove(sName, options) {
        if (!options) {
            var options = {};
        }
        options.expires = new Date();
        setCookie(sName, '', options);
    }
}

new gfOutdatedBrowser();
