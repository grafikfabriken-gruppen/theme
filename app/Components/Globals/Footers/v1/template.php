<footer class="gf-global-footer">
	<div class="container">

		<?php if ($image_url = $this->get_logo()) : ?>
		<div class="row">
			<div class="col">
				<a href=" <?= get_home_url(); ?>" class="gf-logo">
					<img src=" <?= $image_url ?>">
				</a>
			</div><!-- /.col -->
		</div><!-- /.row -->
		<?php endif; ?>

		<div class="row copyright-container">
			<div class="col">
				<?= __("Webbproduktion:", "grafikfabriken"); ?> <a href="https://grafikfabriken.nu"><img
						src="<?= GF_THEME_ASSETS_URL ?>/images/grafikfabriken_dark.svg" alt=""></a>
			</div><!-- .col -->
		</div><!-- .row copyright-container -->

	</div><!-- /.container -->
</footer><!-- /.gf-global-footer -->