<?php

namespace GF\Components\Globals\Footers\v1;

use function GF\Utils\optionsPageFactory;


final class Component extends \GF\Models\Component
{

	/**
	 * Create a global settings page
	 *
	 * @return void
	 */
	public function theme_hooks()
	{
		optionsPageFactory()->create_theme_sub_page(__('Footer', 'grafikfabriken'), 'footer-settings', $this->option_fields, $this->id);
		//Register menu
		// register_nav_menus(array(
		// 	'footer-menu' => __('Footer menu', 'grafikfabriken'),
		// ));
	}

	/**
	 * Get the logo
	 *
	 * @return string
	 */
	public function get_logo()
	{
		$logo_id = get_field('footer_logo', 'options');
		$url = wp_get_attachment_image_url($logo_id, 'full');

		return $logo_id ? (strpos($url, '.svg') === false ? aq_resize($url, 300) : $url) : '';
	}

	/**
	 * Get the oprion
	 *
	 * @param string $key
	 * @return string
	 */
	public function get($key)
	{
		return get_field('footer_' . $key, 'options');
	}
}