import $ from 'jquery'

class Input {
    constructor(elm) {
        this.input = $(elm)

        this.addClassClosestFormGroup = () =>
            this.input.closest('.form-group').addClass('js-input-active')
        this.removeClassClosestFormGroup = () =>
            this.input.closest('.form-group').removeClass('js-input-active')

        this.inputHasValue()
        this.focus()
        this.blur()

        $(document).on('ajaxComplete', this.inputHasValue())
        $(window).on('load', this.inputHasValue())
    }

    inputHasValue() {
        if (this.input.val() != '' && this.input.length > 0) {
            this.addClassClosestFormGroup()
        } else {
            this.removeClassClosestFormGroup()
        }
    }

    focus() {
        this.input.focus(() => {
            this.addClassClosestFormGroup()
        })
    }

    blur() {
        this.input.blur(() => {
            this.inputHasValue()
        })
    }
}

class BootstrapSelect {
    constructor() {
        this.select = 0
        this.addLabelClass
        $(window).on('load', this.addLabelClass)
        $(document).on('ajaxComplete', this.addLabelClass)
    }

    addLabelClass() {
        this.select = $('.bootstrap-select')
        if (this.select.prev('label').length > 0) {
            this.select.prev('label').addClass('js-boostrap-select-label')
        }
    }
}

$(document).on('ready', () => {
    $(':input,textarea').each((index, elm) => {
        const input = new Input($(elm))
    })

    // Adds selected-changed class to Bootstrap-select when changed
    $('body').on('change', '.selectpicker, .bootstrap-select', e => {
        const $this = $(e.currentTarget)

        $this
            .closest('.bootstrap-select')
            .find('.filter-option-inner-inner')
            .addClass('selected-changed')
    })

    const select = new BootstrapSelect()
})
