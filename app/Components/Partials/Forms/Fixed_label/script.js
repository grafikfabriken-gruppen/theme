import $ from 'jquery'

class BootstrapSelect {
    constructor() {
        this.select = 0
        this.addLabelClass
        $(window).on('load', this.addLabelClass)
        $(document).on('ajaxComplete', this.addLabelClass)
    }

    addLabelClass() {
        this.select = $('.bootstrap-select')
        if (this.select.prev('label').length > 0) {
            this.select.prev('label').addClass('js-boostrap-select-label')
        }
    }
}

$(document).on('ready', () => {
    // Adds selected-changed class to Bootstrap-select when changed
    $('body').on('change', '.selectpicker, .bootstrap-select', e => {
        const $this = $(e.currentTarget)

        $this
            .closest('.bootstrap-select')
            .find('.filter-option-inner-inner')
            .addClass('selected-changed')
    })

    const select = new BootstrapSelect()
})
