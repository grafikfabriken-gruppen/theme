<?php
namespace GF\Components\Partials\Pagination\v1;

class Component extends \GF\Models\Component
{
    
    /**
     * Paged
     *
     * @var integer
     */
    public $paged = 0;

    /**
     * Pages
     *
     * @var integer
     */
    public $pages = 0;

    /**
     * Get page link
     *
     * @param integer $i
     * @return string
     */
    public function get_page_link(int $i):string{
        return get_pagenum_link($i);
    }

    /**
     * Actions performed before rendering
     *
     * @author Jonathan Persson <jonathan@grafikfabriken.nu>
     * @version 1.0
     */
    public function before_render():void
    {
        global $paged;
        global $wp_query;

        // check if current page is the first page
        if (empty($paged)) $paged = 1;

        $this->paged = $paged;
        $this->pages = $wp_query->max_num_pages ? $wp_query->max_num_pages : 1;

    }

    /**
     * Render partials
     *
     * @return string
     * @author Jonathan Persson <jonathan@grafikfabriken.nu>
     * @version 1.0
     */
    public function render():string
    {
        return $this->pages > 1 ? parent::render() : "";
    }
}