<?php /** @var \GF\Components\Partials\Pagination\v1\Component $this */ ?>
<div class="p-pagination-v1">

	<?php if ($this->paged > 1) : ?>
	<a href="<?= $this->get_page_link($this->paged - 1); ?>" class="text-senary">
		<i class="fas fa-caret-left"></i>
	</a>
	<?php endif; ?>

	<?php for ($i = 1; $i <= $this->pages; $i++):?>
	<?php if ($this->pages != 1 && (!($i >= $this->paged + 5 || $i <= $this->paged - 5) || $this->pages <= 10)): ?>
	<?php if($this->paged === $i): ?>
	<a href="<?= get_pagenum_link($i) ?>" class="text-primary">
		<?= $i ?>
	</a>
	<?php else:?>
	<a href="<?= get_pagenum_link($i) ?>" class="text-senary">
		<?= $i ?>
	</a>
	<?php endif; ?>
	<?php endif;?>
	<?php endfor; ?>

	<?php if ($this->paged < $this->pages): ?>
	<a href="<?= get_pagenum_link($this->paged + 1) ?>" class="text-senary">
		<i class="fas fa-caret-right"></i>
	</a>
	<?php endif; ?>

</div>