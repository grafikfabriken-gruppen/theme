import $ from 'jquery';
import Cookiebar from './assets/js/cookiebar.model';
import CookieTable from './assets/js/cookietable.model';

$(document).ready(e => {
    new Cookiebar();

    $('body')
        .find('table.gdpr-cookies')
        .each((index, value) => {
            new CookieTable($(value));
        });
});
