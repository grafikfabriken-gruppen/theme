<?php
namespace GF\Components\Partials\Plugins\Cookie;

use function GF\Utils\optionsPageFactory;

class Component extends \GF\Models\Component
{

	/**
	 * Title
	 * 
	 * @var string
	 */
	public $content;

	/**
	 * Btn text
	 * 
	 * @var string
	 */
	public $button_label;

	/**
	 * Options in use
	 *
	 * @var array
	 */
	public $options_in_use = [
		'content',
		'button_label'
	];

	/**
	 * Constructor
	 */
	public function __construct()
	{	

		/**
		 * Pretty name
		 */
		$this->pretty_name = __('GDPR Cookies', 'grafikfabriken');

		/**
		 * GDPR Cookies
		 * 
		 */
		add_shortcode('gdpr_cookies', array($this, 'display_cookie_table'));
		add_action('wp_footer', array($this, 'render'));

	}

	/**
	 * Ovveride render
	 *
	 * @return string
	 */
	public function render()
	{
		$html = parent::render();
		echo $html;
		return '';
	}

	/**
	 * Display tables
	 *
	 * @param array $_args
	 * @return string
	 */
	public static function display_cookie_table($_args)
	{
		ob_start();
		include '/shortcode.php';
		return ob_get_clean();
	}

	/**
	 * Theme hooks
	 *
	 * @return void
	 */
	public function theme_hooks()
	{
		//Add options page
		optionsPageFactory()->create_theme_sub_page(__('GDPR-Cookies', 'grafikfabriken'), 'gdpr-cookie-settings', $this->option_fields);
	}

	/**
	 * Get content
	 * 
	 * @return string
	 */
	public function get_content()
	{
		return apply_filters("the_content", $this->content);
	}

}