<?php /** @var \GF\Components\Globals\Plugins\Cookie\Component $this */ ?>

<section id="gf-gdpr-cookie-bar">
	<div class="container">
		<div class="row justify-content-center mb">
			<div class="col-12 col-sm">
				<div id="gf-gdpr-cookie-content">
					<?= $this->get_content(); ?>
				</div><!-- #gf-gdpr-cookie-content -->
			</div><!-- .col -->
			<div class="col-12 col-sm-auto">
				<div id="gf-gdpr-cookie-button">
					<button type="button" id="gf-gdpr-accept-button" class="btn btn-primary">
						<?= $this->button_label; ?>
					</button><!-- #gf-gdpr-accept-button.btn btn-primary -->
				</div><!-- #gf-gdpr-cookie-button -->
			</div><!-- .col -->
		</div><!-- .row -->
	</div><!-- .container -->
</section><!-- #gf-gdpr-cookie-bar -->