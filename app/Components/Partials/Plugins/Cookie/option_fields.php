<?php /** @var \GF\Components\Globals\Plugins\Cookie\Component $this */

return [
    [
        'key' => 'cookie_content',
        'label' => __('Content', "grafikfabriken"),
        'name' => 'cookie_content',
        'type' => 'wysiwyg',
        'default_value' => __('<p>To improve the experience on the site, we use cookies. <a href="#readmore">Read more about cookies & privacy policy</a></p>', 'grafikfabriken'),
        'class_key' => 'content'
    ],
    [
        'key' => 'button_label',
        'label' => __('Accept button label', 'grafikfabriken'),
        'name' => 'button_label',
        'type' => 'text',
        'default_value' => __('I agree', 'grafikfabriken'),
        'class_key' => 'button_label'
    ]
];