<?php /** @var \GF\Components\Globals\Plugins\Cookie\Component $this */ ?>
<table class="gdpr-cookies">
    <thead>
        <tr>
            <th><?= __('Name', 'gf-gfdpr-cookies'); ?></th>
            <th><?= __('Publisher', 'gf-gfdpr-cookies'); ?></th>
            <th><?= __('Type', 'gf-gfdpr-cookies'); ?></th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>