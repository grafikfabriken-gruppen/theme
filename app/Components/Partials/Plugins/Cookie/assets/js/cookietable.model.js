import GDPR_Cookie from './cookie.model';

class CookieTable {
    constructor(elm) {
        this.elm = elm;

        this.groups = require('../json/groups.json');

        this.known_types = require('../json/known_types.json');

        this.global_cookies = this.getAllCookies();

        this.cookies = [];

        try {
            this.mapCookies();
            this.buildTable();
        } catch (error) {
            console.log(error);
        }
    }

    getAllCookies() {
        var pairs = document.cookie.split(';');
        var cookies = {};
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            cookies[(pair[0] + '').trim()] = unescape(pair.slice(1).join('='));
        }
        return cookies;
    }

    mapCookies() {
        Object.keys(this.global_cookies).forEach(_cookie_key => {
            var group = {name: 'wordpress', group: 'other', provider: 'Okänd'};
            this.known_types.forEach(nt => {
                if (_cookie_key.startsWith(nt.name)) {
                    group = nt;
                }
            });

            this.cookies.push(
                new GDPR_Cookie(_cookie_key, group.provider, this.groups[group.group])
            );
        });
    }

    buildTable() {
        this.cookies.sort((a, b) => {
            if (a.provider < b.provider) return -1;
            if (a.provider > b.provider) return 1;
            return 0;
        });

        this.cookies.forEach(_cookie => {
            let html = _cookie.getHtml();
            html.appendTo(this.elm.find('tbody'));
        });
    }
}

// module.exports = CookieTable;
export default CookieTable;
