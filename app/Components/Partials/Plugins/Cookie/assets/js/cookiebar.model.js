class Cookiebar {
    constructor() {
        this.bodyClass = 'cookie-bar-visible';

        this.btn = $('#gf-gdpr-accept-button');
        this.bar = $('#gf-gdpr-cookie-bar');
        this.cookie_name = 'gf-gdpr-accept';

        this.btn.on('click', this.accept.bind(this));

        this.evalulate();
    }

    /**
     * Evalulate if wee nedd to show cookies?
     */
    evalulate() {
        let accepted = gf.cookie(this.cookie_name);

        if (!accepted) {
            $('body').addClass(this.bodyClass);
            this.bar.show();
        }
    }

    /**
     * Accept our terms!
     */
    accept() {
        gf.cookie(this.cookie_name, true);
        this.bar.hide();
        $('body').removeClass(this.bodyClass);
    }
}

// module.exports = Cookiebar;
export default Cookiebar;
