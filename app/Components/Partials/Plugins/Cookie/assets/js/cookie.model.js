class GDPR_Cookie {
    constructor(_name, _provider, _group) {
        this.name = _name;
        this.provider = _provider;
        this.group = _group;
    }

    getHtml() {
        let tr = $('<tr/>');

        Object.keys(this).forEach(_key => {
            $('<td/>').html(this[_key]).appendTo(tr);
        });

        return tr;
    }
}

module.exports = GDPR_Cookie;
