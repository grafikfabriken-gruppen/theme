<div class="quantity-wrapper form-group">
	<?php if ($args['max_value'] && $args['min_value'] == $args['max_value']) : ?>
	<?php else : ?>
		<div class="adjust decrease">
			<i class="fas fa-minus"></i>
		</div>
	<?php endif; ?>
