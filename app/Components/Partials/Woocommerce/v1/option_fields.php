<?php

/** @var \GF\Components\Partials\Settings\contact_information $this */

return array(
    // Global contact information
    array(
        'key' => 'woocommerce_thank_you',
        'label' => __('Thankyou page', 'grafikfabriken'),
        'name' => 'option_woocommerce_thank_you_tab',
        'type' => 'tab',
        'include_key' => 'woocommerce_tab'
    ),
    array(
        'key' => 'woocommerce_thank_you_content',
        'label' => __('Content', 'grafikfabriken'),
        'name' => 'option_woocommerce_thank_you_content',
        'type' => 'wysiwyg',
        'class_key' => 'thank_you_content',
        'instructions' => __('This message will be shown on the thank you-page. Use <code>{order_id}</code> to show the ID of the order', 'grafikfabriken')
    ),
    array(
        'key' => 'woocommerce_klarna_placeholer',
        'label' => __('Klarna placeholder', 'grafikfabriken'),
        'name' => 'option_woocommerce_klarna_placeholer',
        'type' => 'textarea',
        'class_key' => 'klarna_placeholer',
        'instructions' => __('Add the klarna placeholder visual code from merchant enter.', 'grafikfabriken')
    ),
    array(
        'key' => 'woocommerce_klarna_placeholer_script',
        'label' => __('Klarna placeholder script', 'grafikfabriken'),
        'name' => 'option_woocommerce_klarna_placeholer_script',
        'type' => 'textarea',
        'class_key' => 'klarna_placeholer_script',
        'instructions' => __('Klarna placeholder script', 'grafikfabriken')
    ),
    
);