<?php
namespace GF\Components\Partials\Woocommerce\v1 {

use function GF\Utils\optionsPageFactory;

final class Component extends \GF\Models\Component{

		/**
		 * Thank you page content
		 *
		 * @var string
		 */
		public $thank_you_content;


		public function __construct(){

			// add_filter( 'woocommerce_add_to_cart_fragments', array($this, 'cart_fragments') );

			add_filter('woocommerce_thankyou_order_received_text', array($this, 'thank_you_content'));
			add_action('woocommerce_single_product_summary', array($this, 'display_klarna_placeholder'), 31);

			add_action('wp_body_open', array($this, 'wp_body_open'));
			add_filter( 'body_class', array( $this, 'remove_svea_class' ), 11, 2 );
		}

		/**
		 * Remove the svea class
		 *
		 * @param string[] $classes
		 * @return string[]
		 */
		public function remove_svea_class($classes){
			foreach ($classes as $key => $value) {
				if ($value == 'svea-checkout-confirmation-page') {
					unset($classes[$key]);
					break;
				}
			}
			return $classes;
		}

		public function display_klarna_placeholder(){
			echo get_field('option_woocommerce_klarna_placeholer', 'option');
		}

		public function wp_body_open(){
			echo get_field('option_woocommerce_klarna_placeholer_script', 'option');
		}

		/**
		 * Thank you content after purcahse
		 *
		 * @return string
		 */
		public function thank_you_content(){
			$content = get_field('woocommerce_thank_you_content', 'option');
			$order_id = get_query_var('order-received');
			$content = str_replace('{order_id}', $order_id, $content);

			return apply_filters('the_content', $content);
		}


		 /**
		 * When all components are installed, create a options page
		 *
		 * @return void
		 */
		public function installed(){
			$fields = require 'option_fields.php';
			optionsPageFactory()->create_theme_sub_page(__('WooCommerce', 'grafikfabriken'), 'woocommerce', $fields, 'wc');
		}





	}
}


// Use for the global namespace
namespace {
	/**
	* Output the quantity input for add to cart forms.
	*
	* @param  array           $args Args for the input.
	* @param  WC_Product|null $product Product.
	* @param  boolean         $echo Whether to return or echo|string.
	*
	* @return string
	*/
   function woocommerce_quantity_input( $args = array(), $product = null, $echo = true ) {
	   if ( is_null( $product ) ) {
		   $product = $GLOBALS['product'];
	   }

	   $defaults = array(
		   'input_id'     => uniqid( 'quantity_' ),
		   'input_name'   => 'quantity',
		   'input_value'  => '1',
		   'classes'      => apply_filters( 'woocommerce_quantity_input_classes', array( 'input-text', 'qty', 'text' ), $product ),
		   'max_value'    => apply_filters( 'woocommerce_quantity_input_max', -1, $product ),
		   'min_value'    => apply_filters( 'woocommerce_quantity_input_min', 0, $product ),
		   'step'         => apply_filters( 'woocommerce_quantity_input_step', 1, $product ),
		   'pattern'      => apply_filters( 'woocommerce_quantity_input_pattern', has_filter( 'woocommerce_stock_amount', 'intval' ) ? '[0-9]*' : '' ),
		   'inputmode'    => apply_filters( 'woocommerce_quantity_input_inputmode', has_filter( 'woocommerce_stock_amount', 'intval' ) ? 'numeric' : '' ),
		   'product_name' => $product ? $product->get_title() : '',
	   );

	   $args = apply_filters( 'woocommerce_quantity_input_args', wp_parse_args( $args, $defaults ), $product );

	   // Apply sanity to min/max args - min cannot be lower than 0.
	   $args['min_value'] = max( $args['min_value'], 0 );
	   $args['max_value'] = 0 < $args['max_value'] ? $args['max_value'] : '';

	   // Max cannot be lower than min if defined.
	   if ( '' !== $args['max_value'] && $args['max_value'] < $args['min_value'] ) {
		   $args['max_value'] = $args['min_value'];
	   
		}

	   	ob_start();
	   	include 'quantity/before-quantity.php';
	   	wc_get_template( 'global/quantity-input.php', $args );
	   	include 'quantity/after-quantity.php';
	   
		if ( $echo ) {
			echo ob_get_clean(); // WPCS: XSS ok.
		} else {
			return ob_get_clean();
		}
   }

	function woocommerce_form_field($key, $args, $value = null)
	{

		$defaults = array(
			'type' => 'text',
			'label' => '',
			'description' => '',
			'placeholder' => '',
			'maxlength' => false,
			'required' => false,
			'autocomplete' => false,
			'id' => $key,
			'class' => array(),
			'label_class' => array(),
			'input_class' => array(),
			'return' => false,
			'options' => array(),
			'custom_attributes' => array(),
			'validate' => array(),
			'default' => '',
			'autofocus' => '',
			'priority' => '',
		);

		$args = wp_parse_args($args, $defaults);
		$args = apply_filters('woocommerce_form_field_args', $args, $key, $value);

		if ($args['required']) {
			$args['class'][] = 'validate-required';
			$required = '&nbsp;<abbr class="required" title="' . esc_attr__('required', 'imrab') . '">*</abbr>';
		} else {
			$required = '&nbsp;<span class="optional">(' . esc_html__('optional', 'imrab') . ')</span>';
		}

		if (is_string($args['label_class'])) {
			$args['label_class'] = array($args['label_class']);
		}

		if (is_null($value)) {
			$value = $args['default'];
		}

		// Custom attribute handling.
		$custom_attributes = array();
		$args['custom_attributes'] = array_filter((array)$args['custom_attributes'], 'strlen');

		if ($args['maxlength']) {
			$args['custom_attributes']['maxlength'] = absint($args['maxlength']);
		}

		if (!empty($args['autocomplete'])) {
			$args['custom_attributes']['autocomplete'] = $args['autocomplete'];
		}

		if (true === $args['autofocus']) {
			$args['custom_attributes']['autofocus'] = 'autofocus';
		}

		if ($args['description']) {
			$args['custom_attributes']['aria-describedby'] = $args['id'] . '-description';
		}

		if (!empty($args['custom_attributes']) && is_array($args['custom_attributes'])) {
			foreach ($args['custom_attributes'] as $attribute => $attribute_value) {
				$custom_attributes[] = esc_attr($attribute) . '="' . esc_attr($attribute_value) . '"';
			}
		}

		if (!empty($args['validate'])) {
			foreach ($args['validate'] as $validate) {
				$args['class'][] = 'validate-' . $validate;
			}
		}

		$field = '';
		$label_id = $args['id'];
		$sort = $args['priority'] ? $args['priority'] : '';

		// <div class="form-row">
		// <div class="col-md-6">
		// <div class='form-group'>
		// <label>Användarnamn</label>
		// <input type='text' class='form-control' name='test' placeholder='Användarnamn' />
		// <small class='form-text text-danger'>Lorem, ipsum dolor.</small><!-- /.form-text text-danger -->
		// </div><!-- /.form-group -->
		// </div><!-- /.col-md-6 -->
		$field_container = '<p class="form-row %1$s" id="%2$s" data-priority="' . esc_attr($sort) . '">%3$s</p>';

		// Default
		$field_container = '<div class="form-row">
			<div class="col-12" id="%2$s" data-priority="' . esc_attr($sort) . '">
				<div class="form-group">%3$s</div>
			</div>
		</div>';

		if (in_array('form-row-first', $args['class'])) {
			$field_container = '<div class="form-row">
			<div class="col-md-6" id="%2$s" data-priority="' . esc_attr($sort) . '">
				<div class="form-group">%3$s</div>
			</div>';
		} elseif (in_array('form-row-last', $args['class'])) {
			$field_container = '<div class="col-md-6" id="%2$s" data-priority="' . esc_attr($sort) . '">
				<div class="form-group">%3$s</div>
			</div>
		</div>';
		}

		switch ($args['type']) {
			case 'country':
				$countries = 'shipping_country' === $key ? WC()->countries->get_shipping_countries() : WC()->countries->get_allowed_countries();

				if (1 === count($countries)) {

					$field .= '<strong>' . current(array_values($countries)) . '</strong>';

					$field .= '<input type="hidden" name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '"
			value="' . current(array_keys($countries)) . '" ' . implode(' ', $custom_attributes) . '
			class="country_to_state" readonly="readonly" />';
				} else {

					$field = '<select name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '"
			class="selectpicker country_to_state country_select ' . esc_attr(implode(' ', $args['input_class'])) . '" ' . implode(' ', $custom_attributes) . '>
			<option value="">' . esc_html__('Select a country&hellip;', 'imrab') . '</option>';

					foreach ($countries as $ckey => $cvalue) {
						$field .= '<option value="' . esc_attr($ckey) . '" ' . selected($value, $ckey, false) . '>' . $cvalue . '
			</option>';
					}

					$field .= '
		</select>';

					$field .= '<noscript><button type="submit" name="woocommerce_checkout_update_totals"
				value="' . esc_attr__('Update country', 'imrab') . '">' . esc_html__('Update country', 'imrab') .
						'</button></noscript>';
				}

				break;
			case 'state':
				/* Get country this state field is representing */
				$for_country = isset($args['country']) ? $args['country'] : WC()->checkout->get_value('billing_state' === $key ?
					'billing_country' : 'shipping_country');
				$states = WC()->countries->get_states($for_country);

				if (is_array($states) && empty($states)) {

					$field_container = '<p class="form-row %1$s" id="%2$s" style="display: none">%3$s</p>';

					$field .= '<input type="hidden" class="hidden" name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '"
			value="" ' . implode(' ', $custom_attributes) . ' placeholder="' . esc_attr($args['placeholder']) . '"
			readonly="readonly" />';
				} elseif (!is_null($for_country) && is_array($states)) {

					$field .= '<select name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '"
			class="selectpicker state_select ' . esc_attr(implode(' ', $args['input_class'])) . '" ' . implode(' ', $custom_attributes) . '
			data-placeholder="' . esc_attr($args['placeholder']) . '">
			<option value="">' . esc_html__('Select an option&hellip;', 'imrab') . '</option>';

					foreach ($states as $ckey => $cvalue) {
						$field .= '<option value="' . esc_attr($ckey) . '" ' . selected($value, $ckey, false) . '>' . $cvalue . '
			</option>';
					}

					$field .= '
		</select>';
				} else {

					$field .= '<input type="text" class="input-text ' . esc_attr(implode(' ', $args['input_class'])) . '"
			value="' . esc_attr($value) . '" placeholder="' . esc_attr($args['placeholder']) . '"
			name="' . esc_attr($key) . '"
			id="' . esc_attr($args['id']) . '" ' . implode(' ', $custom_attributes) . ' />';
				}

				break;
			case 'textarea':
				$field .= '<textarea name="' . esc_attr($key) . '"
			class="input-text form-control' . esc_attr(implode(' ', $args['input_class'])) . '"
			id="' . esc_attr($args['id']) . '"
			placeholder="' . esc_attr($args['placeholder']) . '" ' . (empty($args[' custom_attributes']['rows'])
					? ' rows="2"' : '') . (empty($args['custom_attributes']['cols']) ? ' cols="5"' : '') . implode(
					' ',
					$custom_attributes
				) . '>' . esc_textarea($value) . '</textarea>';
				break;
			case 'checkbox':
				$field = '<label class="checkbox ' . implode(' ', $args['label_class']) . '" ' . implode(
					' ',
					$custom_attributes
				) . '>
						<input type="' . esc_attr($args['type']) . '" class="input-checkbox ' . esc_attr(implode(
					' ',
					$args['input_class']
				)) . '" name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '" value="1" '
					. checked($value, 1, false) . ' /> ' . $args['label'] . $required . '</label>';
				break;
			case 'text':
			case 'password':
			case 'datetime':
			case 'datetime-local':
			case 'date':
			case 'month':
			case 'time':
			case 'week':
			case 'number':
			case 'email':
			case 'url':
			case 'tel':
				$field .= '<input type="' . esc_attr(
					$args['type']
				) . '" class="form-control ' . esc_attr(implode(' ', $args['input_class'])) . '" name="' .
					esc_attr($key) . '" id="' . esc_attr($args['id']) . '" placeholder="' . esc_attr($args['placeholder'])
					. '"  value="' . esc_attr($value) . '" ' . implode(' ', $custom_attributes) . ' />';
				break;
			case 'select':
				$field = '';
				$options = '';
				if (!empty($args['options'])) {
					foreach ($args['options'] as $option_key => $option_text) {
						if ('' === $option_key) {
							// If we have a blank option, select2 needs a placeholder.
							if (empty($args['placeholder'])) {
								$args['placeholder'] = $option_text ? $option_text : __('Choose an option', 'imrab');
							}
							$custom_attributes[] = 'data-allow_clear="true"';
						}
						$options .= '<option value="' . esc_attr($option_key) . '" ' . selected($value, $option_key, false) . '>' . esc_attr($option_text) . '</option>';
					}

					$field .= '<select name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '" class="selectpicker select ' . esc_attr(implode(' ', $args['input_class'])) . '" ' . implode(' ', $custom_attributes) . ' data-placeholder="' . esc_attr($args['placeholder']) . '">
							' . $options . '
						</select>';
				}

				break;
			case 'radio':
				$label_id = current(array_keys($args['options']));

				if (!empty($args['options'])) {
					foreach ($args['options'] as $option_key => $option_text) {
						$field .= '<input type="radio" class="input-radio ' . esc_attr(implode(' ', $args['input_class'])) . '" value="' . esc_attr($option_key) . '" name="' . esc_attr($key) . '" ' . implode(' ', $custom_attributes) . ' id="' . esc_attr($args['id']) . '_' . esc_attr($option_key) . '"' . checked($value, $option_key, false) . ' />';
						$field .= '<label for="' . esc_attr($args['id']) . '_' . esc_attr($option_key) . '" class="radio ' . implode(' ', $args['label_class']) . '">' . $option_text . '</label>';
					}
				}

				break;
		}

		if (!empty($field)) {
			$field_html = '';

			if ($args['label'] && 'checkbox' !== $args['type']) {
				$label_class = '';
				if ($args['type'] == 'country') {
					$label_class = 'custom-select-label';
				}
				$field_html .= '<label for="' . esc_attr($label_id) . '" class="' . $label_class  . esc_attr(implode(' ', $args['label_class'])) . '">' . $args['label'] . $required . '</label>';
			}

			// $field_html .= '<span class="woocommerce-input-wrapper">' . $field;
			$field_html .= $field;

			if ($args['description']) {
				$field_html .= '<span class="description" id="' . esc_attr($args['id']) . '-description" aria-hidden="true">' . wp_kses_post($args['description']) . '</span>';
			}

			// $field_html .= '</span>';

			$container_class = esc_attr(implode(' ', $args['class']));
			$container_id    = esc_attr($args['id']) . '_field';
			$field           = sprintf($field_container, $container_class, $container_id, $field_html);
		}

		// if($args['type'] == 'country'){
		//     dpr( htmlentities( $field_html),2);
		// }

		/**
		 * Filter by type.
		 */
		$field = apply_filters('woocommerce_form_field_' . $args['type'], $field, $key, $args, $value);

		/**
		 * General filter on form fields.
		 *
		 * @since 3.4.0
		 */
		$field = apply_filters('woocommerce_form_field', $field, $key, $args, $value);

		if ($args['return']) {
			return $field;
		} else {
			echo $field; // WPCS: XSS ok.
		}
	}



	function wc_dropdown_variation_attribute_options( $args = array() ) {
		$args = wp_parse_args(
			apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ),
			array(
				'options'          => false,
				'attribute'        => false,
				'product'          => false,
				'selected'         => false,
				'name'             => '',
				'id'               => '',
				'class'            => '',
				'show_option_none' => __( 'Choose an option', 'woocommerce' ),
			)
		);

		// Get selected value.
		if ( false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product ) {
			$selected_key     = 'attribute_' . sanitize_title( $args['attribute'] );
			$args['selected'] = isset( $_REQUEST[ $selected_key ] ) ? wc_clean( wp_unslash( $_REQUEST[ $selected_key ] ) ) : $args['product']->get_variation_default_attribute( $args['attribute'] ); // WPCS: input var ok, CSRF ok, sanitization ok.
		}

		$options               = $args['options'];
		$product               = $args['product'];
		$attribute             = $args['attribute'];
		$name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
		$id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
		$class                 = $args['class'];
		$show_option_none      = (bool) $args['show_option_none'];
		$show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

		if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
			$attributes = $product->get_variation_attributes();
			$options    = $attributes[ $attribute ];
		}

		$html = '<label>' . wc_attribute_label( $attribute ) . '</label>';
		$html.= '<select id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">';
		$html .= '<option value="">' . __('Select value', 'grafikfabriken') . '</option>';
		
		if ( ! empty( $options ) ) {
			if ( $product && taxonomy_exists( $attribute ) ) {
				// Get terms if this is a taxonomy - ordered. We need the names too.
				$terms = wc_get_product_terms(
					$product->get_id(),
					$attribute,
					array(
						'fields' => 'all',
					)
				);

				foreach ( $terms as $term ) {
					if ( in_array( $term->slug, $options, true ) ) {
						$html .= '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name, $term, $attribute, $product ) ) . '</option>';
					}
				}
			} else {
				foreach ( $options as $option ) {
					// This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
					$selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
					$html    .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option, null, $attribute, $product ) ) . '</option>';
				}
			}
		}

		$html .= '</select>';

		echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args ); // WPCS: XSS ok.
	}
}