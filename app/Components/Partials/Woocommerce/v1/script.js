import $ from 'jquery'

class WooCommerce_fixes {
    constructor() {
        this.setupBinds()
    }

    setupBinds() {
        $('.builder_block_sections_woocommerce_cart, .product').on(
            'click',
            '.quantity-wrapper .adjust',
            this.adjustQty.bind(this)
        )
        $('.builder_block_sections_woocommerce_checkout, .product-name').on(
            'click',
            '.quantity-wrapper .adjust',
            this.adjustQty.bind(this)
        )



        if($('.product').on('change keyup', 'input[name="quantity"]', this.inputChange.bind(this)));



        $( ".single_variation_wrap" ).on( "show_variation", function ( e, variation ) {
            // Fired when the user selects all the required dropdowns / attributes
            let $addToCartBtn = $(e.currentTarget).closest('form').find('.ajax_add_to_cart');

            
            if( $addToCartBtn.length > 0 ){
                $addToCartBtn.attr('data-product_id', variation.variation_id);
            }
        } );

        // $(document.body).on('')
        // $(document.body).on("added_to_cart", this.addedToCart.bind(this));
    }

    // /**
    //  *
    //  * @param {event} e
    //  */
    // addedToCart(e) {
    //   $(".gn-mini-cart").addClass("minicartopen");

    //   setTimeout(() => {
    //     $(".gn-mini-cart").removeClass("minicartopen");
    //   }, 2000);
    // }

    /**
     * Check for ajax-button when the input for quantity is changed
     * 
     * @param {event} e Javascript Event
     */
    inputChange(e){
        
        let $addToCartBtn = $(e.currentTarget).closest('form').find('.ajax_add_to_cart');
        if( $addToCartBtn.length > 0 ){
            $addToCartBtn.attr('data-quantity', Math.max(0, $(e.currentTarget).val()));
        }
    }
    /**
     * Adjust the quantity
     * @param {event} e
     */
    adjustQty(e) {
        let $adjust = $(e.currentTarget)
        let $input = $adjust.closest('.quantity-wrapper').find('input.qty')

        let step = $input.attr('step') ? parseInt($input.attr('step')) : 1

        let val = step
        if ($adjust.hasClass('decrease')) val = -step

        let old_val = parseInt($input.val())

        let new_val = old_val + val

        let _min_val = $input.attr('min')
        let min_value = _min_val ? _min_val : 0

        let _max_val = $input.attr('max')
        let max_value = _max_val ? _max_val : Infinity

        if (new_val < min_value) new_val = min_value // Don't go below the minimum
        if (new_val > max_value) new_val = max_value // Don't go above the maximum

        $input.val(new_val).trigger('change')
    }
}

$(document).on('ready', () => {
    let wcf = new WooCommerce_fixes()
})
