<?php
namespace GF\Components\Partials\Example\v1;

final class Component extends \GF\Models\Component
{

    /**
     * Title
     *
     * @var string
     */
    public $title;

    /**
     * Subtitl
     *
     * @var string
     */
    public $sub_title;

    /**
     * Content
     * 
     * @var string
     */
    public $content;

    /**
     * Constructor
     */
    public function __construct()
    {
        
        $this->pretty_name = __("Example", "grafikfabriken");
        
    }


    /**
     * Get content
     *
     * @return string
     */
    public function get_content():string
    {
        return apply_filters('the_content', $this->content);
    }

}