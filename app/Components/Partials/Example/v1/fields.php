<?php 
use function GF\Utils\fieldsFactory;
/** @var \GF\Components\Partials\Example\v1\Component $this */


$key = "partials_example_v1_component";

$fields = array(
    array(
        'key' => $key . '_title',
        'label' => __('Title', 'grafikfabriken'),
        'name' => 'title',
        'type' => 'text',
        "custom_title_tag" => true
    ),
    array(
        'key' => $key . '_sub_title',
        'label' => __('Sub title', 'grafikfabriken'),
        'name' => 'sub_title',
        'type' => 'text',
        "custom_title_tag" => true
    ),
    array(
        'key' => $key . '_content',
        'label' => __('Content', 'grafikfabriken'),
        'name' => 'content',
        'type' => 'wysiwyg',
    ),
);

$settings = array();

return fieldsFactory()->get_content_and_settings_fields($key, $fields, $settings, true, false);