<?php

/** @var \GF\Components\Partials\Example\v1\Component $this */ ?>
<div class="card shadow bg-primary text-white">
    <div class="card-header">
        <?= $this->get_title_tag('title', 'h1'); ?>
    </div>
    <div class="card-body">
        <?= $this->get_title_tag('sub_title', 'h5', array('lorem', 'ipsum')); ?>
        <?= $this->get_content(); ?>
    </div>
    <div class="card-footer">
        <a href="<?= home_url(); ?>" class="btn btn-secondary"><?= __('Read more', 'grafikfabriken'); ?></a>
    </div>
</div>