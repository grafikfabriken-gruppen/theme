<?php

use function \GF\Utils\fieldsFactory;

/** @var \GF\Components\Partials\Puff\Main\Component $this */


$key = "partials_puff_main";

$fields = [
	[
		"key" => $key . "_sub_title",
		"label" => __("Small title", "grafikfabriken"),
		"name" => "sub_title",
		"type" => "text",
		"custom_title_tag" => "h6"
	],
	[
		"key" => $key . "_title",
		"label" => __("Title", "grafikfabriken"),
		"name" => "title",
		"type" => "text",
		"required" => 1,
		"custom_title_tag" => true
	],
	[
		"key" => $key . "_content",
		"label" => __("Content", "grafikfabriken"),
		"name" => "content",
		"type" => "wysiwyg",
		"media_upload" => 0,
		"required" => 1,
	],
	[
		"key" => $key . "_image_id",
		"label" => __("Image", "grafikfabriken"),
		"instructions" => __("Recommended imagesize 570x350px", "grafikfabriken"),
		"name" => "image_id",
		"type" => "image",
		"return_format" => "id"
	],
	[
		"key" => $key . "_link_text",
		"label" => __("Button text", "grafikfabriken"),
		"name" => "link_text",
		"type" => "text",
	],
	[
		"key" => $key . "_link_url",
		"label" => __("Button url", "grafikfabriken"),
		"name" => "link_url",
		"type" => "text",
	],
	[
		"key" => $key . "_link_new_window",
		"label" => __("Open link in new window", "grafikfabriken"),
		"name" => "link_new_window",
		"type" => "true_false",
	],
];

// dpr($this->enable_acf_settings,2);

return fieldsFactory()->get_content_and_settings_fields($key, $fields, array(), true, false);
