<?php
namespace GF\Components\Partials\Puff_card\Main;

class Component extends \GF\Models\Component
{

	/**
	 * Title
	 * 
	 * @var string
	 */
	public $title;

	/**
	 * Sub title
	 * 
	 * @var string
	 */
	public $sub_title;

	/**
	 * Content
	 * 
	 * @var string
	 */
	public $content;

	/**
	 * Link text
	 * 
	 * @var string
	 */
	public $link_text;

	/**
	 * Link URL
	 * 
	 * @var string
	 */
	public $link_url;

	/**
	 * If link should be opened in a new window
	 * 
	 * @var bool
	 */
	public $link_new_window;

	/**
	 * Image id
	 *
	 * @var int
	 */
	public $image_id;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->pretty_name = __('Card', 'grafikfabriken');
	}

	/**
	 * Get background image
	 *
	 * @param integer $width
	 * @param integer $height
	 * @return string
	 */
	public function get_background_image(int $width = 1122, int $height = 400): string
	{

		if ($this->image_id > 0) {
			if ($full_image_url = wp_get_attachment_image_url($this->image_id, 'full')) {
				return sprintf('background-image:url(%s)', aq_resize($full_image_url, $width, $height, true, true, true));
			}
		}

		return '';
	}

	/**
	 * Get image tag
	 *
	 * @param integer $width
	 * @param integer $height
	 * @return string
	 */
	public function get_image_tag(int $width = 1122, $height = 400)
	{
		if ($this->image_id > 0) {
			if ($full_image_url = wp_get_attachment_image_url($this->image_id, 'full')) {
				$alt_text = get_post_meta($this->image_id, '_wp_attachment_image_alt', true);
				return sprintf('<img src="%s" alt="%s">', aq_resize($full_image_url, $width, $height, true, true, true), $alt_text);
			}
		}

		return '';
	}

	/**
	 * Check if block is renderable
	 * 
	 * @return bool
	 */
	public function is_renderable()
	{
		return (is_string($this->title) && strlen($this->title) > 0) || (is_string($this->content) && strlen($this->content) > 0);
	}

	/**
	 * Get content
	 * 
	 * @return string
	 */
	public function get_content()
	{
		return apply_filters("the_content", $this->content);
	}

	/**
	 * Get button
	 * @param string $class
	 * @return string
	 */
	public function get_button($class = 'btn-primary', $type = 'a')
	{
		ob_start();

		if (is_string($this->link_url) && strlen($this->link_url) > 0 && is_string($this->link_text) && strlen($this->link_text) > 0) {
			$target = $this->link_new_window ? "target=\"_blank\"" : "";

			$href = '';
			if ($type == 'a') {
				$href = 'href="' . $this->link_url .'"';
			}
			echo "<". $type ." ". $href ." " . $target . " class=\"btn " . $class . "\">" . $this->link_text . "</".$type.">";
		}

		return ob_get_clean();
	}

	/**
	 * Render the thing
	 * 
	 * @return string
	 */
	public function render()
	{
		return $this->is_renderable() ? parent::render() : "";
	}
}