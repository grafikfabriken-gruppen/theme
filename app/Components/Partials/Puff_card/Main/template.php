<?php /** @var \GF\Components\Partials\Puff_card\Main\Component $this */ ?>
<div class="p-puff_card-v1 card">
	<div class="card-body">
		<i class="fas fa-<?= $this->icon; ?> card-icon"></i>
		<h4 class="card-title"><?= $this->title; ?></h4><!-- .card-title -->
		<p class="card-desc"><?= $this->desc; ?></p><!-- .card-desc -->
		<?= $this->button_component->render() ?>
	</div><!-- .card-body -->
</div><!-- /.p-offices_card-v1 .card -->