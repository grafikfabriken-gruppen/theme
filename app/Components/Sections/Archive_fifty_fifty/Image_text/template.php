<?php /** @var \GF\Components\Sections\Archive_fifty_fifty\Image_text\Component $this */ ?>

<?php if (array_has_items($this->posts)) : ?>


<div class="row title-row">
	<div class=" col-auto">
		<h2><?= $this->get_title(); ?></h2>
	</div><!-- .col-auto -->
</div><!-- .row -->

<?php $index = 0; ?>
<?php foreach ($this->posts as $card) : ?>
<div class="row mb news-card <?= $index % 2 === 0 ? "flex-row-reverse" : ""; ?>">
	<div class="col-12 col-lg-6 d-flex">
		<?= $card->get_image_tag(600, 300); ?>
	</div><!-- .col-12 col-lg-6 -->
	<div class="col-12 col-lg-6">
		<?= $card->get_title_tag("sub_title", 'default-text'); ?>
		<?= $card->get_title_tag("title", 'h4'); ?>
		<div class="truncate-box">
			<?= $card->get_content(); ?>
		</div><!-- .truncate-box -->
		<?= $card->get_button(); ?>
	</div><!-- .col-12 col-lg-6 -->
</div><!-- /.row -->
<?php $index++; ?>
<?php endforeach; ?>
<div class="row justify-content-center">
	<div class="col">
		<?= $this->pagination_component->render(); ?>
	</div><!-- .col -->
</div><!-- .row -->
<?php else : ?>
<div class="row">
	<div class="col">
		<h2><?= $this->get_title(); ?></h2>
		<p>
			<?= __('We could not find any posts..', 'grafikfabriken'); ?>
		</p>
	</div><!-- .col -->
</div><!-- .row -->
<?php endif; ?>