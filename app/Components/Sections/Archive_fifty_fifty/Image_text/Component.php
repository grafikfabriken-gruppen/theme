<?php
namespace GF\Components\Sections\Archive_fifty_fifty\Image_text;

use GF\Utils\Array_Utils;

final class Component extends \GF\Models\Component_PB_Block
{
    

    public $tile;

    /**
     * Flip
     *
     * @var boolean
     */
    public $flip = false;

    /**
     * Cache component
     * 
     * @var bool
     */
    public $cache = true;
    
    /**
     * Card component
     *
     * @var \GF\Components\Partials\Puff_card\Main\Component
     */
    public $card_component;

    /**
     * Pagination component
     *
     * @var \GF\Components\Partials\Pagination\v1\Component
     */
    public $pagination_component;

    /**
     * Controller
     *
     * @var \App\Controllers\Post_Controller
     */
    public $controller;

    /**
     * Controller name
     *
     * @var string
     */
    public $controller_name = '\\App\\Controllers\\Post_Controller';

    public $posts;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->dynamic_only = true;

        $this->dynamic_pages = ['archive', 'category'];

        $this->pretty_name = __("Archive", "grafikfabriken");

        $this->partials = [
            "card_component" => [
                "folder" => "Puff_card",
                "id" => "Main"
            ],
            "pagination_component" => [
                "folder" => "Pagination",
                "id" => "v1"
            ]
        ];

    }

    /**
     * Get title
     *
     * @return void
     */
    public function get_title()
    {

        if (is_search()) {
            $s = sanitize_text_field($_REQUEST['s']);
            return sprintf(__('Search results for "%s"', 'grafikfabriken'), $s);
        } else if (is_category()) {
            return single_cat_title('', false);
        } else if (is_archive()) {
            return $this->title;
        } else {
            return $this->title;
        }
    }

    public function before_render()
    {

        $posts = $this->controller->get_posts_from_the_loop();

        if(array_has_items($posts)){
            $this->posts = Array_Utils::map_models_to_component($posts, $this->sub_components["card_component"]);
        }
        

    }

    
    
}