<?php

use function \GF\Utils\fieldsFactory;

/** @var \GF\Components\Sections\Archive_fifty_fifty\Image_text\Component $this */


$key = "sections_archive_fifty_fifty_image_text";

$fields = array(
    array(
        'key' => $key . '_title',
        'label' => __('Title', 'grafikfabriken'),
        'name' => 'title',
        'type' => 'text',
    ),
);

return fieldsFactory()->get_content_and_settings_fields($key, $fields);