<?php
namespace GF\Components\Sections\Content\Block;

final class Component extends \GF\Models\Component_PB_Block
{
    
    /**
     * Controller name
     *
     * @var string
     */
    public $controller_name = '\\GF\\Controllers\\Block_Controller';

    /**
     * Block ID
     *
     * @var int
     */
    public $block_id;

    /**
     * Controller
     *
     * @var \GF\Controllers\Block_Controller
     */
    public $controller;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dynamic_pages = ['archive', 'category', '404'];
        $this->pretty_name = __("Block", "grafikfabriken");
        $this->custom_wrapper = true;
        
    }


    /**
     * Get template html
     *
     * @return string
     */
    public function get_template()
    {
        return '';
    }


    /**
     * Render the thing
     * 
     * @return string
     */
    public function render()
    {
        return $this->block_id > 0 ? \GF\Utils\pageBuilder()->render(false, $this->block_id) : '';
        // return $this->is_renderable() ? parent::render() : "";
    }

}