<?php 
use function GF\Utils\fieldsFactory;
/** @var \GF\Components\Sections\Coworkers\v1\Component $this */


$args = array(
    'post_type' => 'block',
    'posts_per_page' => 999
);
$blocks = get_posts($args);

$choises = array('' => __('Select block', 'grafikfabriken'));
if (array_has_items($blocks)) {
    foreach ($blocks as $block) {
        $choises[$block->ID] = $block->post_title;
    }
}

$fields = array(
    array(
        'key' => 'sections_content_block_id',
        'label' => __('Block', 'grafikfabriken'),
        'name' => 'block_id',
        'type' => 'select',
        'choices' => $choises
    ),
    
    
);

return fieldsFactory()->get_content_and_settings_fields('sections_content_block', $fields);