<?php /** @var \GF\COmponents\Sections\Example\v1\Component $this */ ?>
<div class="row justify-content-center">
    <div class="col-12">
        <?= $this->get_title_tag('title', 'h1'); ?>
        <?= $this->get_title_tag('sub_title', 'h5', array('lorem', 'ipsum')); ?>
        <?= $this->get_content(); ?>
    </div>
</div>
<?php if (array_has_items($this->cards)): ?>
    <div class="row mt-5">
        <?php foreach ($this->cards as $card) : ?>
            <div class="col-6">
                <?= $card->render(); ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>