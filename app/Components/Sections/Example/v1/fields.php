<?php 
use function GF\Utils\fieldsFactory;
/** @var \GF\Components\Sections\Example\v1\Component $this */


$key = "sections_example_v1_component";

$fields = array(
    array(
        'key' => $key . '_title',
        'label' => __('Title', 'grafikfabriken'),
        'name' => 'title',
        'type' => 'text',
        "custom_title_tag" => true
    ),
    array(
        'key' => $key . '_sub_title',
        'label' => __('Sub title', 'grafikfabriken'),
        'name' => 'sub_title',
        'type' => 'text',
        "custom_title_tag" => true
    ),
    array(
        'key' => $key . '_content',
        'label' => __('Content', 'grafikfabriken'),
        'name' => 'content',
        'type' => 'wysiwyg',
    ),
    array(
        'key' => $key . '_cards',
        'label' => __('Cards', 'grafikfabriken'),
        'name' => 'cards',
        'type' => 'repeater',
        'sub_fields' => $this->get_sub_component_fields('cards', false),
        'layout' => 'row'
    ),
    
);

$settings = array(
    array(
        'key' => $key . '_show_title',
        'label' => __('Show title', 'grafikfabriken'),
        'name' => 'show_title',
        'type' => 'true_false',
    ),
);

return fieldsFactory()->get_content_and_settings_fields($key, $fields, $settings, true, true);