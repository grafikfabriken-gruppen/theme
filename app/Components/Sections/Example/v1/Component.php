<?php
namespace GF\Components\Sections\Example\v1;

use function PHPSTORM_META\map;

final class Component extends \GF\Models\Component_PB_Block
{

    /**
     * Title
     *
     * @var string
     */
    public $title;

    /**
     * Subtitl
     *
     * @var string
     */
    public $sub_title;

    /**
     * Content
     * 
     * @var string
     */
    public $content;

    /**
     * Cards
     *
     * @var \GF\Components\Partials\Example\v1\Component[]
     */
    public $cards;

    /**
     * Constructor
     */
    public function __construct()
    {

        /**
         * Allow this section behind header
         * 
         * 
         */
        $this->allow_behind_header = true;
        
        $this->pretty_name = __("Example", "grafikfabriken");

        $this->partials = array(
            "cards" => array(
                "folder" => "Example",
                "id" => "v1",
                "type" => "array"
            )
        );

        // $this->dataTypes["cards"] = "array[\\GF\\Components\\Partials\\Example\\v1\\Component]";
        
    }


    /**
     * Get content
     *
     * @return string
     */
    public function get_content():string
    {
        return apply_filters('the_content', $this->content);
    }

}