<?php

/** @var \GF\Components\Sections\Four_Oh_Four\v1\Component $this */ ?>

<div class="row justify-content-center">
	<div class="col-12 col-lg-6">
		<h1><?= $this->title; ?></h1>
		<h4><?= $this->sub_title; ?></h4>
		<?= $this->content; ?>
		<div class="btn-container">
			<?= $this->get_button(); ?><?= $this->get_button(2); ?>
		</div><!-- .btn-container -->
	</div>
</div>