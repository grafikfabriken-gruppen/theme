const merge = require("webpack-merge");
const CopyPlugin = require("copy-webpack-plugin");
const webpackBaseConfig = require("./webpack.common.config.js");

module.exports = merge(webpackBaseConfig, {
    plugins: [
        new CopyPlugin({
            patterns: [{from: "./app/assets/images", to: "./images"}],
        }),
    ],
});
