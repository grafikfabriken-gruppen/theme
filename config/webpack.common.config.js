const localServer = {
    path: "https://webpack.test",
    port: 1337,
};

const webpack = require("webpack");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const WebpackShellPlugin = require("webpack-shell-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const autoprefixer = require("autoprefixer");

console.log(">>> Building theme: " + theme_name);
console.log(">>> Dist output: " + output);

var argv = require("minimist")(process.argv.slice(2));
var theme_name = argv.theme ? path.basename(argv.theme) : "grafikfabriken";
var output = argv.theme ? path.resolve(argv.theme, "dist") : path.resolve("./app", "dist");
var child_theme = argv.theme ? argv.theme : null;

if (child_theme) {
    console.log(">>> Child theme:" + child_theme);
}

const config = {
    entry: {
        app: path.resolve(__dirname, "../app/assets/entry.js"),
    },
    output: {
        publicPath: `/app/themes/${theme_name}/app/dist`,
        filename: "js/[name].js",
        path: output,
    },
    externals: {
        jquery: "jQuery",
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: "babel-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.(s[ac]ss|css)$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: "../",
                        },
                    },
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                            plugins: () => [autoprefixer()],
                        },
                    },
                    "sass-loader",
                ],
            },

            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                exclude: path.resolve(__dirname, "./app/assets/images"),
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "fonts/",
                        },
                    },
                ],
            },
            {
                test: /\.svg$/,
                exclude: path.resolve(__dirname, "./app/assets/fonts"),
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "images/",
                        },
                    },
                ],
            },
            {
                test: /\.(png|gif|jpg|jpeg)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            outputPath: "images/",
                            limit: 8192,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new WebpackShellPlugin({
            onBuildStart: [
                "node vendor/grafikfabriken/constantine/src/build.js" +
                    (child_theme ? " --child_theme=" + child_theme : ""),
            ],
        }),

        new MiniCssExtractPlugin({
            filename: "css/[name].css",
        }),

        new BrowserSyncPlugin({
            proxy: localServer.path,
            port: localServer.port,
            files: [
                "app/Components/**/**/*.php",
                "app/Components/**/**/*.html",
                "vendor/grafikfabriken/constantine/src/Components/**/**/*.php",
                "vendor/grafikfabriken/constantine/src/Components/**/**/*.html",
            ],
        }),

        new webpack.ProvidePlugin({$: "jquery", jQuery: "jquery"}),

        // new CleanWebpackPlugin({}),
    ],
};

module.exports = config;
