const merge = require('webpack-merge');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CssnanoPlugin = require('cssnano-webpack-plugin');
const webpackBaseConfig = require('./webpack.common.config.js');

module.exports = merge(webpackBaseConfig, {
    optimization: {
        minimize: true,
        minimizer: [
            new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i }),
            new TerserPlugin(),
            new OptimizeCSSAssetsPlugin(),
            new CssnanoPlugin(),
        ],
    },
});
