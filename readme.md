## The Grafikfabriken Theme

This is a boilerplate for a ultra fast Wordpress Theme.
Using modern tech as Composer, ES6, Webpack, NodeJs.

## Install the theme

1. Create a new Project with bedrock/sage https://roots.io/bedrock/docs/installing-bedrock/

2. Add dependencys for gf-companion and acf.

2. Create A new theme with composer: `composer create-project grafikfabriken/theme {your-destination}`

3. Setup manifest in the app folder

4. Activate theme and flush permalinks!

5. Run `npm install`

## Having trouble to setup your manifest?

For some inspiration look into example-manifest.json

## Your assets is not loaded correctly?

To generate the autoload file, Visit Settings->permalinks and press update!

## Building assets

`npm run build`

## Watch assets

`npm run watch`

## Building production assets

`npm run build --production`